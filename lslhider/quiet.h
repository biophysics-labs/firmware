/*
 * quiet.h -- functions to (temporarily) suppress all output to stdout and stderr
 */
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

inline int suppress_stdout() {
  fflush(stdout);

  int ret = dup(1);
  int nullfd = open("/dev/null", O_WRONLY);
  // check nullfd for error omitted
  dup2(nullfd, 1);
  close(nullfd);

  return ret;
}

inline void resume_stdout(int fd) {
  fflush(stdout);
  dup2(fd, 1);
  close(fd);
}

inline int suppress_stderr() {
  fflush(stderr);

  int ret = dup(1);
  int nullfd = open("/dev/null", O_WRONLY);
  // check nullfd for error omitted
  dup2(nullfd, 1);
  close(nullfd);

  return ret;
}

inline void resume_stderr(int fd) {
  fflush(stderr);
  dup2(fd, 1);
  close(fd);
}
