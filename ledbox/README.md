This is the source code for the Raspberry PI part of the led driver.
ledd listens for incoming zmq connections on the network interface.
After connecting, ledd will pass commands received on the zmq socket to the
aduino mega, which takes care of controlling the led outputs.

On matlab this is connected to by ledcontroller_pi.m from the biofysia toolbox
on gitlab.science.ru.nl:/marcw/biofysica

The arduino code in this directory may be outdated. Ruurd Lof maintains that software.
