//
//  Hello World server in C++ with azmq and boost
//  Binds REP socket to tcp://*:5555
//  Expects "Hello" from client, replies with "World"
//
//#include <termios.h>
//#include <unistd.h>
#include <memory>
#include <iomanip> 
#include <azmq/socket.hpp>
#include <boost/asio.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/program_options.hpp>


#include <unistd.h>

using namespace std;
using namespace boost;



const char* arduino_dev = "/dev/ttyACM0";

class arduino_serial
{
   asio::io_service io;
   asio::serial_port port;

public:

   arduino_serial(const char *dev) : io(), port(boost::asio::serial_port(io))
   {
       port.open(dev);
       port.set_option(asio::serial_port_base::baud_rate(115200));

       ::tcflush(port.lowest_layer().native_handle(), TCIOFLUSH);
   }

   void read(std::string& line)
   {
      asio::streambuf response;
      asio::read_until(port, response, "\n");
      istream s(&response) ;
      getline(s, line);
   }

   void write(const std::string& line)
   {
      asio::write(port, boost::asio::buffer(line+'\n'));
   }
};

static int verbose = 0;

void parse_options(int argc, const char* argv[])
{
    namespace po = boost::program_options;

    // Declare the supported options.
    po::options_description desc("Usage:");
    desc.add_options()
       ("help,h", "show this message")
       ("verbose,v", "enable verbose output")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
       cout << desc << "\n";
       exit(0);
    }

    if (vm.count("verbose")) {
       verbose = 1;
    }
}

std::string str2hex(const std::string& s)
{
    const static char str2hex_lookup[] = "0123456789abcdef";
    unsigned int i=0,leng=s.length();
    std::stringstream r;
    for(i=0; i<leng; i++)
    {
        r<< str2hex_lookup[ s[i] >> 4 ];
        r<< str2hex_lookup[ s[i] & 0x0f ];
    }
    return r.str();
}


int main(int argc, const char* argv[])
{
    parse_options(argc, argv);

    //  Prepare our context and socket
    asio::io_service ios;
    azmq::socket socket(ios, ZMQ_REP);
    socket.bind("tcp://*:5555");
    arduino_serial arduino(arduino_dev);

    while (true)
    {
        //  Wait for next request from client
        //char request[4096];
        std::array<char,4096> request;

        socket.receive(asio::buffer(request));
std::string str(request.data());
//const char* str=request.data();

        if (verbose)
            cout << "Received: " << str << endl;

        // Send it to the Arduino
        arduino.write(str);

        // Wait for the reply from the Arduino
        std::string result;
        arduino.read(result);
 
        if (verbose)
        {
            cout << "Arduino replies:: " << result << endl
                 <<  str2hex(result) << endl;
        }
        //  Send reply back to client
        socket.send(asio::buffer(result));
    }
    return 0;
}
