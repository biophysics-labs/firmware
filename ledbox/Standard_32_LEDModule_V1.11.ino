//********************************************************************************************************//
//                                    Standard 32 LED module V1.11                                        //
//                              © Radboud University, Biophysics Dept.                                    //    
//                                          By Ruurd Lof                                                  //
//                                        dd. 02-10-2018                                                  //  
//********************************************************************************************************//

//new in this version:
//256 configurations instead of 8
//constants used for Nr_Of_PWMpins, Nr_Of_Leds and Nr_Of_Configs
//timeout time in SerialParseInt set to 1000 ms.
//delay in SerialParseInt set to 100 microsec


boolean test = false;
String VersionString = "Standard_32_LEDModule_V1.11.ino 02-10-2018";

//use of strings for communication
// commands "T", "RV", "RE", "RT", "RC", "RR", "RG", "RI", "WE x", "WT x", "WC x", "WR xxx...xxx","WG xxx...xxx", "WI xxx...xxx",  "RX x"
// echo     "RV ......", "RE x" "RT x" "RC x","RR xxx,.... "OK" "OK" "OK" "OK" or "RX xxxx..."  

String responseString = ""; 
int errorCode = 0; // used as index for ErrorStrings

String ErrorStrings[11] = {"Error 0: No Error", 
                           "Error 1: No 'T', 'R' or 'W' found as first character",
                           "Error 2: No valid character found after 'R' {ECTRGIJ}",
                           "Error 3: No valid character found after 'W' {ECTRGIJ}",
                           "Error 4: Error reading Red LED data", 
                           "Error 5: Error reading Green LED data",
                           "Error 6: Timeout parsing integer", 
                           "Error 7: No newline detected at end of command line", 
                           "Error 8: Error reading Red LED intensities", 
                           "Error 9: Error reading Green LED intensities",
                           "N > 9: Invalid error number"};

const byte newline = 10; 

const int LEDtestpin_Red = 2;
const int LEDtestpin_Grn = 3;
const int Triggerpin = 13;

const int Nr_Of_Leds = 16;

const int LEDpins_Red[Nr_Of_Leds] = {22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52};  // nr. 22 corresponds with LSB, nr. 52 with MSB
const int LEDpins_Grn[Nr_Of_Leds] = {23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53};  // nr. 23 corresponds with LSB, nr. 53 with MSB

const int Nr_Of_PWMpins = 8;
const int PWMpins[Nr_Of_PWMpins]  = {4,5,6,7,8,9,10,11};  

const int Max_PWM_Intensity = 255;

// interface parameters 

const int Nr_Of_Configs = 256;

unsigned int PWM_Intensity[Nr_Of_Configs];         //= {255, 255, 255, 255 ,255 ,255, 255, 255,.....}; //R: "RI"; W: "WI xxx, xxx, ..."

unsigned int Red_Configs[Nr_Of_Configs];          //= {0,0,0,0,0,0,0,0,.....}; // R: "RR"; W: "WR xxx, xxx, ..." 

unsigned int Grn_Configs[Nr_Of_Configs];          //= {0,0,0,0,0,0,0,0,.....}; //R: "RG"; W: "WG xxx, xxx, ..."



unsigned int Current_PWM = 0;
unsigned int Current_Red = 0;
unsigned int Current_Grn = 0;

unsigned int trigger_enabled = 0; // R/W "E" 
unsigned int triggered = 0;       // R/W "T" 
unsigned int trigger_count = 0;   // R/W "C" 

//********************************************************************************************************//
//                                        nthBitFromInt16()
//********************************************************************************************************/

bool nthBitFromInt16(unsigned int aValue, int n) // 0 is LSB
{  
  return (aValue>>n) & 1; // shift n places to right and return the LSB.
}

//********************************************************************************************************//
//                                        SerialParseInt()
//********************************************************************************************************/

boolean TimeOut = false;

void SerialParseInt(boolean &TimeOut, unsigned int &Value)
{
  long TStartParsing = millis(); 
  int i = Serial.parseInt(); delayMicroseconds(100);
  TimeOut = (millis() > TStartParsing + 1000); // generate timeout after 100 ms 
  if (!TimeOut)
  {    
    Value = i;    
    errorCode=0; //no error
  } 
  else
  {
    Value = 0;
    errorCode=6; //Timeout
  }
}

//********************************************************************************************************//
//                                        set output
//********************************************************************************************************//

void shift_registers()
{
  Current_PWM = PWM_Intensity[0];
  Current_Red = Red_Configs[0];
  Current_Grn = Grn_Configs[0];
  for (int i=0; i<(Nr_Of_Configs-1); i++)
  {
    Red_Configs[i] = Red_Configs[i+1];
    Grn_Configs[i] = Grn_Configs[i+1];        
    PWM_Intensity[i] = PWM_Intensity[i+1];    
  }
  Red_Configs[Nr_Of_Configs-1] = 0;
  Grn_Configs[Nr_Of_Configs-1] = 0;
  PWM_Intensity[Nr_Of_Configs-1] = 0;
}
  
void output_values()
{
  set_PWM_values();
  set_LEDpins();
}


//********************************************************************************************************//
//                                        Trigger
//********************************************************************************************************//

void detectTriggerPinChange()
{
  static boolean triggerPinValue;
  boolean aTriggerPinValue = digitalRead(Triggerpin);
  if (aTriggerPinValue != triggerPinValue)
  {
    triggerPinValue = aTriggerPinValue;
    set_Triggered(triggerPinValue);
  }
}

void set_Triggered(unsigned int aTriggered) //hardware trigger
{
  if (aTriggered!= triggered)
  {
    triggered = aTriggered;
    if (triggered)
    {  
      TriggerNow();
    } 
  }
}

void SoftwareTrigger()  //software trigger
{
  TriggerNow();
  responseString = "OK";
}

void TriggerNow()
{
  trigger_count++;
  shift_registers();   
  output_values();
}



//********************************************************************************************************//
//                                        set_LEDpins()
//********************************************************************************************************//

void set_LEDpins()
{
  for (int i=0; i<Nr_Of_Leds; i++)
  {
    digitalWrite(LEDpins_Red[i], !nthBitFromInt16(Current_Red,i));     
    digitalWrite(LEDpins_Grn[i], !nthBitFromInt16(Current_Grn,i)); 
  }  
  if (test)
    {
      Serial.print("RED: "); 
      for (int i=0; i<Nr_Of_Leds; i++)
      {
        Serial.print(nthBitFromInt16(Current_Red,i)); Serial.print("; "); 
      }  
      Serial.print("GRN: ");  
      for (int i=0; i<Nr_Of_Leds; i++)     
      {
        Serial.print(nthBitFromInt16(Current_Grn,i)); Serial.print("; ");
      }
    }  
}


//********************************************************************************************************//
//                                        set_PWM_values()
//********************************************************************************************************//

void set_PWM_values()
{ 
  for (int i=0; i<Nr_Of_PWMpins; i++)
  {
    analogWrite(PWMpins[i], Current_PWM);     
  }  
  if (test)
  {
    Serial.print("PWM intensity: "); Serial.print(Current_PWM); Serial.print("; ");     
  }  
}

//********************************************************************************************************//
//                                        Receive Command
//********************************************************************************************************//

void ReceiveCommand()
{ 
  char cmd = Serial.read(); delay(1);  
  switch (cmd)
  {
    case 'R'  : ReceiveRedLEDData();  
                break;    
    case 'G'  : ReceiveGrnLEDData();  
                break;   
    case 'I'  : ReceivePWMIntensityData();                         
                break;    
    case 'E'  : ReceiveTriggerEnabled();    
                break;
    case 'C'  : ReceiveTriggerCount();
                break;   
    case 'T'  : ReceiveTriggered();      
                break;
    default   : errorCode = 3;
                break;               
  }
}

void ReceiveRedLEDData()
{   
  for (int i=0;i<Nr_Of_Configs;i++)
  {
    SerialParseInt(TimeOut, Red_Configs[i]);   
    if (TimeOut) 
    {
      errorCode = 4;
      return;
    }
  }  
  trigger_count = 0;
  responseString = "OK";  
}

void ReceiveGrnLEDData()
{ 
  for (int i=0;i<Nr_Of_Configs;i++)
  {
    SerialParseInt(TimeOut, Grn_Configs[i]);   
    if (TimeOut) 
    {
      errorCode = 5;
      return;
    }
  }  
  trigger_count = 0;
  responseString = "OK"; 
}

void ReceivePWMIntensityData()
{
  for (int i=0;i<Nr_Of_Configs;i++)
  {  
    SerialParseInt(TimeOut, PWM_Intensity[i]);
    if (TimeOut) 
    {
      errorCode = 8;
      return;
    }
  }  
  trigger_count = 0;
  responseString = "OK";   
}

void ReceiveTriggerCount()
{  
  SerialParseInt(TimeOut,trigger_count);
  if (!TimeOut)
  {
    responseString = "OK"; 
  }
}

void ReceiveTriggerEnabled()
{  
  SerialParseInt(TimeOut,trigger_enabled);
  if (!TimeOut) 
  {
    responseString = "OK"; 
  }
}

void ReceiveTriggered()
{    
  unsigned int triggered_val;
  SerialParseInt(TimeOut,triggered_val);
  if (!TimeOut)
  {
    set_Triggered(triggered_val);
    responseString = "OK"; 
  }
}

void FlushSerialInputBuffer()
{
  int oldErrorCode = errorCode;
  if (oldErrorCode == 0) //older error have prevalence
  {
    errorCode = 7; //no newline detected
  }
  while (Serial.available()) // flush remaining characters
  {
    if (Serial.read() == newline)
    {      
      errorCode = oldErrorCode; //older error remains valid
    }
    delay(1);    
  }
}


//********************************************************************************************************//
//                                           send Request
//********************************************************************************************************//


void sendRequest()
{   
  char cmd = Serial.read(); delay(1);   
  switch(cmd)
  {    
    case 'E'  : sendTriggerEnabled();    
                  break;
    case 'C'  : sendTriggerCount();
                  break;   
    case 'T'  : sendTriggered();      
                  break;  
    case 'R'  : sendDataRed();
                  break;                      
    case 'G'  : sendDataGrn();
                  break; 
    case 'I'  : sendPWMIntensity();
                  break;                                                                             
    case 'X'  : setErrorString(Serial.parseInt());                     
                  break;      
    case 'V'  : sendVersion();
                break;                        
    default   : errorCode = 2; 
                  break;  
  }
}

void sendDataRed()
{
  responseString = "RR ";
  for (int i=0;i<Nr_Of_Configs;i++)
  {
    responseString = responseString + String(" ") + String(Red_Configs[i]);
  }
}

void sendDataGrn()
{
  responseString = "RG ";
  for (int i=0;i<Nr_Of_Configs;i++)
  {
    responseString = responseString + " " + String(Grn_Configs[i]);
  }
}

void sendPWMIntensity()
{
  responseString = "RI ";
  for (int i=0;i<Nr_Of_Configs;i++)
  {
    responseString = responseString + String(" ") + String(PWM_Intensity[i]);
  }
}

void sendTriggerEnabled()
{
  responseString = "RE " + String(trigger_enabled);
}



void sendTriggerCount()
{
  responseString = "RC " + String(trigger_count);
}



void sendTriggered()
{
  responseString = "RT "+ String(triggered);
}

void makeErrorResponseString()
{ 
  responseString = "ER " + String(errorCode);
}

void sendVersion()
{
  responseString = "RV " + VersionString;
}


//********************************************************************************************************//
//                                        Error handling
//********************************************************************************************************//

void setErrorString(int x)
{     
  if ((x > -1) && (x < 10))
  {
    responseString = "RX " + ErrorStrings[x];
  }
  else
  {
    responseString = "RX " + ErrorStrings[10]; 
  }
}

//********************************************************************************************************//
//                                        SerialAvailable()
//********************************************************************************************************//

void ReadFromSerial()
{
  errorCode = 0;
  char cmd;  
  cmd = Serial.read(); delay(1);    
  switch(cmd)
  {
    case 'W'  : ReceiveCommand();
                break;   
    case 'R'  : sendRequest();
                break;
    case 'T'  : SoftwareTrigger();
                break;                       
    default   : errorCode = 1; 
                break;                                                                   
  }
  FlushSerialInputBuffer();
}

//********************************************************************************************************//
//                                        sendResponse()
//********************************************************************************************************//

void sendResponseString()
{
  if (errorCode > 0)
  {
    makeErrorResponseString();
  }
//  if (responseString.length() > 0)
//  {
    Serial.println(responseString); //sends string with newline character
//  }  
}

//********************************************************************************************************//
//                                             Test LEDs
//********************************************************************************************************//

boolean TestRed = false;
boolean TestGrn = false;

void detectTestLEDSwitches()
{ 
  set_TestRed(!digitalRead(LEDtestpin_Red)); // PULL UP and NO --> pin inverted
  set_TestGrn(!digitalRead(LEDtestpin_Grn)); // PULL UP and NO --> pin inverted
}

void set_TestRed(boolean aTest)
{  
  if (TestRed != aTest)
  {    
    TestRed = aTest;
    if (TestRed)
    {
      set_TestLEDs();       
    }
    else
    {
      output_values();
    }
  } 
}

void set_TestGrn(boolean aTest)
{
  if (TestGrn != aTest)
  {       
    TestGrn = aTest;
    if (TestGrn)
    {
      set_TestLEDs();       
    }
    else
    {
      output_values();
    }
  } 
}

void set_TestLEDs()
{
  for (int i=0; i<Nr_Of_PWMpins; i++)
  {
    analogWrite(PWMpins[i], Max_PWM_Intensity);
    for (int j=0; j<Nr_Of_Leds; j++)
    {
      digitalWrite(LEDpins_Red[j], !(TestRed && (!TestGrn)));
      digitalWrite(LEDpins_Grn[j], !(TestGrn && (!TestRed)));
    }                 
  }  
}


void Initialize_Arrays()
{
  for (int i = 0; i < Nr_Of_Configs; i++) // ...initialize it
  {
    Grn_Configs[i] = 0;
  }
  for (int i = 0; i < Nr_Of_Configs; i++) // ...initialize it
  {
    Red_Configs[i] = 0;
  }
  for (int i = 0; i < Nr_Of_Configs; i++) // ...initialize it
  {
    PWM_Intensity[i] = Max_PWM_Intensity;
  }
}  



//********************************************************************************************************//
//                                        setup()
//********************************************************************************************************//


void setup() 
{
  Initialize_Arrays();
  Serial.begin(115200);
  for (int i=0; i<Nr_Of_Leds; i++)
  {
    pinMode(LEDpins_Red[i], OUTPUT);
    pinMode(LEDpins_Grn[i], OUTPUT);
  }
  for (int i=0; i<Nr_Of_PWMpins; i++)
  {
    pinMode(PWMpins[i], OUTPUT);    
  }  
  
  pinMode(LEDtestpin_Red, INPUT_PULLUP); // LED test input
  pinMode(LEDtestpin_Grn, INPUT_PULLUP); // LED test input
  pinMode(Triggerpin, INPUT);  
  if (test) {Serial.println("In test modus");}
}

//********************************************************************************************************//
//                                        loop()
//********************************************************************************************************//

void loop() // loop time < 200 microseconds
{
  if (Serial.available())
  {        
    ReadFromSerial();
    sendResponseString();
  }  
  if (trigger_enabled) 
  {  
    detectTriggerPinChange(); 
  }
  detectTestLEDSwitches();  
}
