#include <memory>
#include <boost/asio/serial_port.hpp> 
#include <boost/asio.hpp> 
 
using namespace boost;
 
const std::string arduino_dev = "/dev/ttyACM0";


std::string arduino_command(asio::serial_port& port, std::string& cmd)
{
   asio::write(port, asio::buffer(cmd));
   // Read 1 character into c, this will block
   // forever if no character arrives.
   boost::asio::streambuf response;
   asio::read_until(port, response, "\n");
   std::string s;
   std::istream r(&response) ;
   std::getline(r, s);
   return s;
}

std::unique_ptr<asio::serial_port> arduino_open(const std::string& dev)
{
   asio::io_service *io = new asio::io_service;
   std::unique_ptr<asio::serial_port> port{new asio::serial_port(*io)} ;

   port->open(dev);
   port->set_option(asio::serial_port_base::baud_rate(115200));
   return port;
}



int main(int, char**)
{
   //std::unique_ptr<asio::serial_port> port { arduino_open(arduino_dev) };
   std::unique_ptr<asio::serial_port> port = arduino_open(arduino_dev);

   while (!std::cin.eof())
   {
      std::string cmd;
      std::cout << "cmd>> " << std::flush;
      std::cin >> cmd;
      cmd += '\n';
      std::string result = arduino_command(*port, cmd);
      std::cout << result << std::endl;
   }

   return 0;
}
