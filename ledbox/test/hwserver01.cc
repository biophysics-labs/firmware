//
//  Hello World server in C++ with azmq and boost
//  Binds REP socket to tcp://*:5555
//  Expects "Hello" from client, replies with "World"
//
#include <memory>
#include <azmq/socket.hpp>
#include <boost/asio.hpp>
#include <boost/asio/buffer.hpp>

#include <unistd.h>

int main()
{
    //  Prepare our context and socket
    boost::asio::io_service ios;
    azmq::socket socket(ios, ZMQ_REP);
    socket.bind ("tcp://*:5555");

    while (true)
    {
        std::array<char,256> request;

        //  Wait for next request from client
        socket.receive(boost::asio::buffer(request));
        std::cout << "Received Hello" << std::endl;

        //  Do some 'work'
        sleep(1);

        //  Send reply back to client
        socket.send(boost::asio::buffer("World\n"));
    }
    return 0;
}
