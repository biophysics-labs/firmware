#include <iostream>
#include <memory>
#include <boost/asio/serial_port.hpp> 
#include <boost/asio.hpp> 
 
using namespace boost;
using namespace std;
 
const char* arduino_dev = "/dev/ttyACM0";

class arduino_serial
{
   boost::asio::io_service io;
   boost::asio::serial_port port;

public:

   arduino_serial(const char *dev) : io(), port(boost::asio::serial_port(io))
   {
       port.open(dev);
       port.set_option(boost::asio::serial_port_base::baud_rate(115200));

   }

   void read(string& line)
   {
      boost::asio::streambuf response; 
      boost::asio::read_until(port, response, "\n");
      istream s(&response) ;
      getline(s, line);
   }

   void write(const string& line)
   {
      boost::asio::write(port, boost::asio::buffer(line+'\n')); 
   }
};


int main(int, char**)
{
   arduino_serial arduino(arduino_dev);

   while (1)
   {
      cout << "cmd>> " << flush;

      string cmd;
      cin >> cmd;
      if (cin.eof())
         break;

      arduino.write(cmd);

      string result;
      arduino.read(result);

      cout << result << endl;
   }

   return 0;
}
