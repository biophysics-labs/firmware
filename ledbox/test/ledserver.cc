//
//  Hello World server in C++
//  Binds REP socket to tcp://*:5555
//  Expects "Hello" from client, replies with "World"
//
#include <zmq.hpp>
#include <string>
#include <iostream>
#include <unistd.h>

#include <boost/asio/serial_port.hpp>
#include <boost/asio.hpp>


std::string arduino_command(asio::serial_port& port, std::string& cmd)
{
   asio::write(port, asio::buffer(cmd));
   // Read 1 character into c, this will block
   // forever if no character arrives.
   boost::asio::streambuf response;
   asio::read_until(port, response, "\n");
   std::string s;
   std::istream r(&response) ;
   std::getline(r, s);
   return s;
}

asio::serial_port arduino_open(std::string& dev)
{
   asio::io_service io;
   asio::serial_port port(io);

   port.open(dev);
   port.set_option(asio::serial_port_base::baud_rate(115200));
   return port;
}
 

const std::string arduino_dev = '/dev/ttyACM0';

int main () {
    //  Prepare our context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REP);
    socket.bind ("tcp://*:5555");

    while (true) {
        zmq::message_t request;

        //  Wait for next request from client
        socket.recv (&request);
        std::cout << "Received Hello" << std::endl;

        //  Do some 'work'
        sleep(1);

        //  Send reply back to client
        zmq::message_t reply (5);
        memcpy (reply.data (), "World", 5);
        socket.send (reply);
    }
    return 0;
}
