//
//  Hello World client in C++
//  Connects REQ socket to tcp://localhost:5555
//  Sends "Hello" to server, expects "World" back
//
#include <zmq.hpp>
#include <string>
#include <iostream>

int main ()
{
    //  Prepare our context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REQ);

    std::cout << "Connecting to hello world server…" << std::endl;
    socket.connect ("tcp://localhost:5555");

    //  Do 10 requests, waiting each time for a response
    for (int request_nbr = 0; request_nbr != 10; request_nbr++) {
        zmq::message_t request (256);

        std::cout << "cmd>> " << std::flush;
        char cmd[256];
        std::cin >> cmd;
        if (std::cin.eof())
           break;
        memcpy (request.data (), cmd, 256);
        std::cout << "Sending: " << cmd << std::endl;
        socket.send (request);

        //  Get the reply.
        zmq::message_t reply;
        socket.recv (&reply);
        char rpl [256] = { 0, };
        memcpy (rpl, reply.data(), reply.size());
        std::cout  << std::endl << "Received: " << std::endl << rpl << std::endl;
    }
    return 0;
}
