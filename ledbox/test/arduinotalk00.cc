#include <boost/asio/serial_port.hpp> 
#include <boost/asio.hpp> 
 
using namespace boost;
 
std::string read_char() {
	asio::io_service io;
	asio::serial_port port(io);
 
	port.open("/dev/ttyACM0");
	port.set_option(asio::serial_port_base::baud_rate(115200));
 
        asio::write(port, asio::buffer("RE\n",3));
	// Read 1 character into c, this will block
	// forever if no character arrives.
        boost::asio::streambuf response;
	asio::read_until(port, response, "\n");
        std::string s;
        std::istream r(&response) ;
        std::getline(r, s);
	return s;
}


int main(int, char**)
{
   std::cout << read_char() << std::endl;
   return 0;

}

