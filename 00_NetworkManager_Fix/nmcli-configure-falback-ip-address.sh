#
# Script to configure a fallback static IP address when no dhcp server present.
# raspberry bookworm with NetworkManager
#
# Make sure the rootfs is writeable at boot time, see /etc/fstab
#
# also change managed in  /etc/NetworkManager/NetworkManager.conf to be true
# [ifupdown]
# managed=true
# 
# You may have to delete the original "Wired connection 1", using nmcli, or by deleting
# '/etc/NetworkManager/system-connections/Wired connection 1.nmconnection'
#

nmcli connection delete eth0_dhcp
nmcli connection delete eth0_static
nmcli connection delete "Wired connection 0"
nmcli connection delete "Wired connection 1"
nmcli connection add \
    con-name eth0_dhcp \
    ifname eth0 \
    type ethernet \
    autoconnect true \
    connection.autoconnect-priority 1 \
    connection.autoconnect-retries 2 \
    ipv6.method disabled \
    ipv4.method auto \
    ipv4.dhcp-timeout 2 \
    ipv4.may-fail no \
    save yes

nmcli connection add \
    con-name eth0_static \
    ifname eth0 \
    type ethernet \
    autoconnect true \
    connection.autoconnect-priority 0 \
    connection.autoconnect-retries -1 \
    ipv6.method disabled \
    ipv4.method manual \
    ip4 169.254.100.100/16 \
    save yes

nmcli connection reload

#   gw4 192.168.1.254 \
#   ipv4.dns <dns_server_#1_ipv4> \
#   +ipv4.dns <dns_server_#2_ipv4> \
