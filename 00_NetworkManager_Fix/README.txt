If link-local networking doesn't work, this can be due to an issue with dhclient and NEtworkManager. The easy fix is to disable NetworkManager and use dhcpcd instead

$ sudo apt install dhcpcd
$ sudo systemctl enable dhcpcd
$ sudo systemctl disable NetworkManager


