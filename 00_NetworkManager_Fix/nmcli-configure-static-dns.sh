#!/bin/bash

nmcli conn modify "eth0_dhcp" ipv4.ignore-auto-dns yes
nmcli conn modify "eth0_dhcp" ipv4.dns  "8.8.8.8 8.8.4.4"
systemctl restart NetworkManager

#cat << EOF >/etc/NetworkManager/conf.d/90-dns-none.conf
#[main]
#dns=none
#EOF
#
#systemctl reload NetworkManager
#
#cat << EOF >/etc/resolv.conf
#nameserver 8.8.8.8
#nameserver 8.8.4.4
#EOF
