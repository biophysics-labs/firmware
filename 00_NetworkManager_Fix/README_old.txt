Fallback static IP address when no dhcp server present. raspberry bookworm with NetworkManager

!!! make sure rootfs in /etc/fstab is rw before doing this !!!

When using a point-to-point connection between eg. an lslder device and a computer, we want an autoconfigured IPv4 address. Since 2023? this didnt work anymore, ehence this workaround.

This sets a *fixed* APIPA address if no DHCP server is present. That's not according to the book, but at least it works if Raspberry OS doesn't do it. 


in /etc/NetworkManager/NetworkManager.conf set managed to true:

[ifupdown]
managed=true

sudo systemctl restart systemd-networkd

run the nmcli-*.sh scripts to create the fallback config and the static dns which is needed for a readonly rootfs
