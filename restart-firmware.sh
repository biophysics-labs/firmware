#!/bin/bash
cd /home/pi/firmware || exit 1
echo 'firmware processes to be restarted:'
pgrep -fa "pi/firmware"
if [ $? -eq 0 ]; then
  echo killing processes now...
  pkill -f "pi/firmware" || exit 1
sleep 2
else
  echo "(none found)"
fi
echo starting firmware processes now...
sudo ./init.d/rc
echo done
