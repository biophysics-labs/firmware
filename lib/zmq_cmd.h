#ifndef ZMQ_CMD_H
#define ZMQ_CMD_H

#include <zmq.hpp>
#include <zmq_addon.hpp>
#include <nlohmann/json.hpp>

namespace zmq_cmd {

class server : public zmq::socket_t
{
public:
    server();
    ~server();
    auto poll(int timeout = 0);
    int receive(std::vector<zmq::message_t>& recv_msgs);
    int receive(std::vector<std::string>& recv_msgs);
    int receive(std::vector<nlohmann::json>& recv_json);
    auto reply(std::vector<zmq::message_t>& rep);
    auto reply(zmq::message_t& rep);
    auto reply(nlohmann::json& rep);
    virtual int process(int nparts, zmq::message_t parts[],  zmq::message_t reply[]);
    virtual int process(int nparts, zmq::message_t parts[], std::string& reply) = 0;
};


} // namespace zmq_cmd
#endif
