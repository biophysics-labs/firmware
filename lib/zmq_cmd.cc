#include "zmq_cmd.h"
#include <iostream>
#include <stdexcept>
#include <nlohmann/json.hpp>

using namespace std;
using namespace zmq_cmd;

server::~server()
{
}

int server::process(int nparts, zmq::message_t parts[],  zmq::message_t reply[])
{
    string sreply = "OK\n";
    return process(nparts, parts, sreply);
}

auto server::poll(int timeout)
{
    zmq::pollitem_t  pollitem{static_cast<void *>(this), 0, ZMQ_POLLIN, 0};
    zmq::poll(&pollitem, 1, chrono::milliseconds{0});
    return ((pollitem.revents & ZMQ_POLLIN) != 0);
}

int server::receive(std::vector<zmq::message_t>& recv_msgs)
{
        const auto ret = zmq::recv_multipart(*this, std::back_inserter(recv_msgs));
        if (!ret)
            return 0;
        else
            return *ret;
}

int server::receive(std::vector<std::string>& recv_strings)
{
    std::vector<zmq::message_t> recv_msgs;
    auto ret = receive(recv_msgs);

    recv_strings.resize(ret);
    auto q = recv_strings.begin();
    for (auto p = recv_msgs.begin(); p!=recv_msgs.end(); p++) 
       *q++ = p->to_string();
    return ret;
}

int server::receive(std::vector<nlohmann::json>& recv_json)
{
    std::vector<zmq::message_t> recv_msgs;
    auto ret = receive(recv_msgs);

    recv_json.resize(ret);
    auto q = recv_json.begin();
    for (auto p = recv_msgs.begin(); p!=recv_msgs.end(); p++) {
       auto s = p->to_string();
       *q++ = nlohmann::json::parse(s);
    }
    return ret;
}

//auto server::reply(std::vector<zmq::message_t>& rep)
//{
// TO BE IMPLEMTENTED
//}

auto server::reply(zmq::message_t& rep)
{
   return send(rep, zmq::send_flags::none);
}

auto server::reply(nlohmann::json& jrep)
{
   zmq::message_t ret(jrep.dump());
   return reply(ret);
}
