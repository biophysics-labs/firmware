#include "zmq_cmd.h"
#include <iostream>
#include <string>

using namespace std;

class my_cmd : public zmq_cmd_processor
{
public:
    virtual int process(int n, zmq::message_t parts[], string &reply);
};

int my_cmd::process(int nparts, zmq::message_t parts[], string &reply)
{
   
   for (int i=0; i<nparts; i++) {
     cout << "\"" << parts[i].to_string() << "\" ";
   }
   cout << endl;
   reply = "OK!\n";
   return 0;
}


int main()
{
    my_cmd cp; 

    try {
      while (true) 
        cp.poll();
    }
    catch (exception& error) {
        cerr << "zmq_cmd_server:: at cp.poll() : "
             << error.what() << endl;
    }

}
