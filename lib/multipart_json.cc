#include <iostream>
#include <zmq.hpp>
#include <zmq_addon.hpp>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

int main()
{
    zmq::context_t ctx;
    zmq::socket_t sock(ctx, zmq::socket_type::rep);
    sock.bind("tcp://*:5555");

    const std::chrono::milliseconds pollin_timeout{100};
    zmq::pollitem_t pollitem{static_cast<void *>(sock), 0, ZMQ_POLLIN, 0};


    while (true)
    {
        zmq::poll(&pollitem, 1, pollin_timeout);
        if (!(pollitem.revents & ZMQ_POLLIN))
           continue;

        std::vector<zmq::message_t> recv_msgs;
        const auto ret = zmq::recv_multipart(sock, std::back_inserter(recv_msgs));
        if (!ret)
            return 1;
        std::cout << "Got " << *ret
                  << " messages" << std::endl;

        std::vector<std::string> recv_strings;
        for (auto p = recv_msgs.begin(); p!=recv_msgs.end(); p++) {
            recv_strings.push_back(p->to_string());
        }

        //json::object_t j;
        json j;
        json jresult;
        try {   
            for (auto p = recv_strings.begin(); p!=recv_strings.end(); p++) {
               std::string s = *p;
               std::cout << "message: " << s << std::endl;
               j = json::parse(s);
               for (auto jp = j.begin(); jp !=j.end(); jp++)
                  std::cout << "part: " << *jp << std::endl;
               std::cout << "cmd: " << j["cmd"].get<std::string>() << std::endl;
               std::cout << "arg: " << j["arg"] << std::endl;
               std::cout << "arg: " << j["darg"].get<double>() << std::endl;
            }
            jresult["result"] = "ok";
        }
        catch (json::exception& e) {
            std::cerr << std::endl << "error: " << e.what() << '\n'
                  << "exception id: " << e.id << std::endl;
            jresult["result"] = "error";
            jresult["line"] = j;
            jresult["what"] = e.what();
        }
        zmq::message_t zresult(jresult.dump());
        sock.send(zresult,zmq::send_flags::none);
    }
    return 0;
}
