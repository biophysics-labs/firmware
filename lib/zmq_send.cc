#include <iostream>
#include <string>
#include <zmq.hpp>
#include <zmq_addon.hpp>

using namespace std;

int main() {
    zmq::context_t ctx(1);
    zmq::socket_t socket(ctx, zmq::socket_type::req);
    socket.connect("tcp://localhost:5555");

    cout << "Enter one 0MQ message part per line. Terminate multipart message "
            "with an empty line, EOF to exit"
         << endl;
    for (string line; getline(cin, line);) {
        cout << "read: \"" << line << '\"' << endl;
        for (string next_line; getline(cin, next_line);) {
            cout << "read: \"" << next_line << '\"' << endl;
            if (next_line == "") {
                cout << "send: \"" << line << "\" (done)" << endl;
                zmq::message_t msg(line);
                socket.send(msg, zmq::send_flags::none);
                zmq::message_t reply;
                socket.recv (reply, zmq::recv_flags::none);
                cout << "received: " << reply.to_string() << endl;
                break;
            }
            else {
                cout << "send: \"" << line << "\" (+more)" << endl;
                zmq::message_t msg(line);
                line = next_line;
                socket.send(msg, zmq::send_flags::sndmore);
            }
        }
    }
    return 0;
}
