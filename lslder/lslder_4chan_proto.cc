// sendgpioev.cc -- GW/20180417
//
// This program offers four LSL event streams, each triggered by positive edges on
// raspberry pi digital inputs 4,17,22,27
// The stream type is "Digital Events - $HOSTNAME", where HOSTNAME is the ip hostname of
// the raspberry. This is to uniquely identify devices on the network.
// The data sent with the events is an int8 with value 1, with 1 indicating a rising edge.
// Falling edges can be sent with a 0 value. However this is not implemented in this
// program.
//
// Dependencies: labstreaminglayer C++ and wiringPi

#include <lsl_cpp.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <mutex>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <wiringPi.h>

using namespace lsl;

stream_outlet *outlet[4];
std::mutex lsl_mutex;
typedef char int8;

void gpioInterrupt(int channel)
{
    std::lock_guard<std::mutex> guard(lsl_mutex);
#ifdef DEBUG
    std::cout << channel << std::endl;
#endif
    int edge=1;
    outlet[channel]->push_sample(&edge);
}

void gpioInterrupt_4()
{
    gpioInterrupt(0); // gpio4 has input label 1
}

void gpioInterrupt_17()
{
    gpioInterrupt(1);
}

void gpioInterrupt_22()
{
    gpioInterrupt(2);
}

void gpioInterrupt_27()
{
    gpioInterrupt(3);
}

int main(int argc, char* argv[]) {
        piHiPri(99);  // shortcut for running at real time priority
        
        // configure inputs 
        wiringPiSetupGpio();
        pinMode(4, INPUT);
        pinMode(17, INPUT);
        pinMode(22, INPUT);
        pinMode(27, INPUT);
        pullUpDnControl(4, PUD_UP);
        pullUpDnControl(17, PUD_UP);
        pullUpDnControl(22, PUD_UP);
        pullUpDnControl(27, PUD_UP);


        char hostname[255];
        gethostname(hostname,255);
        for (int i=0; i<4; i++)
        {
           // make a new stream_info and open an outlet with it
           char info_name[255];
           sprintf(info_name, "Digital Events %d", i+1);

           char info_type[255];
           sprintf(info_type,"Digital Events @ %s",hostname);

           stream_info info(info_name, info_type, 1, lsl::IRREGULAR_RATE,
              lsl::cf_int8,"Raspberry Pi Digital Event Recorder");
           outlet[i] = new stream_outlet(info);
        }

        // The 4N35 optocoupler at the input inverts the signal, we want to
        // detect rising edges in the input signal, so here we program INT_EDGE_FALLING.
        wiringPiISR(4, INT_EDGE_FALLING, &gpioInterrupt_4);
        wiringPiISR(17, INT_EDGE_FALLING, &gpioInterrupt_17);
        wiringPiISR(22, INT_EDGE_FALLING, &gpioInterrupt_22);
        wiringPiISR(27, INT_EDGE_FALLING, &gpioInterrupt_27);

        // send data forever
        while(true) 
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        return 0;
}

