// lslder_uni.cc -- GW/20220310
//
// LSL Digital Event Recorder
//
// This program offers a maximum of eight LSL event streams, each triggered by
// positive edges on raspberry pi GPIO digital inputs. Visual feedback is
// provided bij eight LEDs. The stream type is "Digital Events - $HOSTNAME",
// where HOSTNAME is the ip hostname of the raspberry. This is to uniquely
// identify devices on the network. The data sent with the events is an int8
// with value 1, with 1 indicating a rising edge. Falling edges can be sent with
// a 0 value. However this is not implemented in this program.
//
// Dependencies: labstreaminglayer C++

// CHANGES
// GW/20180716 initial version for 8-channel LSL Digital Event Recorder
// GW/20210210 remove deprecated wiringPi and use pigpio instead
// TODO: use ISR tick for debounce timing
// TODO: add support to register falling edges
// GW/20220310 add support for sending string markers
// GW/20220324 change ISR mechanism to Alert functions in pigpio
//             -> much better latency en less missing events at high rates
// ----------- add support for changing the LSL name and type published on the
// network

#include "esp32serial.h"
#include "led.h"
#include "simplequeue.h"
#include "sound_detector/sound_detector.h"
#include "zmq_cmd.h"
#include <chrono> // std::chrono::seconds
#include <cstring>
#include <iomanip>
#include <iostream>
#include <lsl_cpp.h>
#include <map>
#include <memory>
//#include <boost/asio.hpp>
//#include <boost/asio/buffer.hpp>
#include <boost/program_options.hpp>
#include <mutex>
#include <nlohmann/json.hpp>
#include <pigpio.h>
#include <sched.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <thread> // std::this_thread::sleep_for
#include <unistd.h>
#include <zmq.hpp>
//#include "zhelpers.hpp"

//#define IGNACIO

using namespace lsl;
using namespace std;
using namespace boost;
using json = nlohmann::json;

enum lslder_config_t {
    LSLDER_UNKNOWN = 0,
    LSLDER_1CHAN,
    LSLDER_4CHAN,
    LSLDER_8CHAN,
    LSLDER_OTOCONTROL,
    LSLDER_OTOCONTROL_v1,
    LSLDER_OTOCONTROL_v4,
};

std::map<string, lslder_config_t> lslder_config_map{
    {"unknown", LSLDER_UNKNOWN}, {"1chan", LSLDER_1CHAN},
    {"4chan", LSLDER_4CHAN},     {"8chan", LSLDER_8CHAN},
    {"oto", LSLDER_OTOCONTROL},
};

string lslder_config_name;
lslder_config_t lslder_config;
int lslder_config_version = 0;

int verbose = 0;
int debug = 0;

string info_name_markers;
string info_type_markers;
string info_sourceid_markers;

esp32serial *sd_serial;
const char *esp32_port = "/dev/ttyS0";
const int esp32_baudrate = 115200;
void parse_options(int argc, const char *argv[]) {
    namespace po = boost::program_options;

    // Declare the supported options.
    po::options_description desc("Usage");
    desc.add_options()("help,h", "show this message")(
        "config-type,c", po::value<string>(),
        "set the hardware configuration type: 1chan,4chan,8chan,oto")(
        "config-version,V", po::value<int>(),
        "set the hardware configuration version")(
        "lsl-name-markers", po::value<string>(),
        "set the name of the LSL marker stream")(
        "lsl-type-markers", po::value<string>(),
        "set the type of the LSL marker stream")(
        "lsl-sourceid-markers", po::value<string>(),
        "set the sourceid of the LSL marker stream")(
        "verbose,v", "enable verbose output")("debug,d",
                                              "enable debuggin output");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        exit(0);
    }

    if (vm.count("verbose")) {
        verbose = 1;
    }

    if (vm.count("debug")) {
        debug = 1;
    }

    if (vm.count("config-type")) {
        lslder_config_name = vm["config-type"].as<string>();
        lslder_config = lslder_config_map[lslder_config_name];
        if (lslder_config == 0) {
            cout << "error: unknown config-type \"" << lslder_config_name
                 << "\"\n";
            cout << desc << "\n";
            exit(1);
        }
    }
    else {
        cout << "error: no config-type provided \n";
        cout << desc << "\n";
        exit(1);
    }

    if (vm.count("config-version")) {
        lslder_config_version = vm["config-version"].as<int>();
    }

    if (vm.count("lsl-name-markers")) {
        info_name_markers = vm["lsl-name-markers"].as<string>();
    }
    else {
        char hostname[255];
        gethostname(hostname, 255);
        ostringstream s;
        s << "Markers @ " << hostname;
        info_name_markers = s.str();
    }

    if (verbose)
        cout << "LSL name is: " << info_name_markers << endl;

    if (vm.count("lsl-type-markers"))
        info_type_markers = vm["lsl-type-markers"].as<string>();
    else
        info_type_markers = "Digital Markers";

    if (verbose)
        cout << "LSL type is: " << info_type_markers << endl;

    if (vm.count("lsl-sourceid-markers"))
        info_sourceid_markers = vm["lsl-sourceid-markers"].as<string>();
    else {
        char hostname[255];
        gethostname(hostname, 255);
        ostringstream s;
        s << argv[0] << "@" << hostname;
        info_sourceid_markers = s.str();
    }
    if (verbose)
        cout << "LSL sourceid is: " << info_sourceid_markers << endl;
}

const int maxgpio = 54; // BCM chip has 54 GPIO
int ngpio;
const int *gpioinp;
bool gpioinp_invert[maxgpio]; // tells us if gpio input redings should be inverted to match levels at BNC
int nled;
const int *ledgpio;
int default_debouncetime = 0;

void init_gpio_config(lslder_config_t config) {
    for (int i=0; i<maxgpio; i++)
      gpioinp_invert[i] = true; // default: inverse logic because of optocouplers

    switch (config) {
    case LSLDER_1CHAN:
        // 1 channel portable version without LEDs
        ngpio = 1;
        const static int _gpioinp_1chan[] = {8};
        nled = 0;
        static int _ledgpio_1chan[] = {};
        ledgpio = 0;
        gpioinp = _gpioinp_1chan;
        ledgpio = _ledgpio_1chan;
        default_debouncetime = 0;
        break;

    case LSLDER_4CHAN:
        // 4-chan version with 4 LEDs
        ngpio = 4;
        static int _gpioinp_4chan[] = {15, 18, 23, 24};
        nled = 4;
        static int _ledgpio_4chan[] = {2, 3, 14, 5};
        gpioinp = _gpioinp_4chan;
        ledgpio = _ledgpio_4chan;
        default_debouncetime = 0;
        break;

    case LSLDER_8CHAN:
        // 8-chan version with 8 LEDs
        ngpio = 8;
        static int _gpioinp_8chan[] = {12, 7, 8, 25, 24, 23, 18, 15};
        nled = 8;
        static int _ledgpio_8chan[] = {21, 20, 16, 13, 5, 14, 3, 2};
        gpioinp = _gpioinp_8chan;
        ledgpio = _ledgpio_8chan;
        default_debouncetime = 0;
        break;

    case LSLDER_OTOCONTROL:
        switch (lslder_config_version) {
        case 1:
            // OtoControl version with 4 LEDs, 2 analog , 1 push button and 1
            // BNC input v1 is PCB prototype version 1 (Dec 2023)
            ngpio = 4;
            static int _gpioinp_oto_v1[] = {3, 2, 10, 9};
            nled = 4;
            static int _ledgpio_oto_v1[] = {4, 17, 27, 22};
            gpioinp = _gpioinp_oto_v1;
            ledgpio = _ledgpio_oto_v1;
            default_debouncetime = 0;
            break;

        case 4:
            // OtoControl version with 4 LEDs, 2 analog , 1 push button and 1
            // BNC input v4 is PCB prototype version 4 (Mai 2024)
            ngpio = 4;
            static int _gpioinp_oto_v4[] = {2, 3, 23, 18};
            nled = 4;
            static int _ledgpio_oto_v4[] = {4, 17, 27, 22};
            gpioinp = _gpioinp_oto_v4;
            ledgpio = _ledgpio_oto_v4;
            gpioinp_invert[gpioinp[0]] = false; // do not invert audio L
            gpioinp_invert[gpioinp[1]] = false; // do not invert audio R
            gpioinp_invert[gpioinp[2]] = false; // do not invert button input
            default_debouncetime = 0;
            sd_serial = new esp32serial(esp32_port, esp32_baudrate, verbose);
            break;
        default:
            cout << "error: otocontrol boards need a config-version to be "
                    "specified"
                 << endl;
            exit(1);
        }
        break;

    case LSLDER_UNKNOWN:
    default:
        cout << "error: unknown configuration" << endl;
        exit(1);
    }
    assert(ngpio <= maxgpio);
}

int gpiomap[maxgpio];

void init_gpiomap() {
    // map BNC input channel wiring 0..ngpio-1 to BCM GPIO
    for (int bnc_input = 0; bnc_input < ngpio; bnc_input++) {
        int bcmgpio = gpioinp[bnc_input];
        gpiomap[bcmgpio] = bnc_input;
    }
}

std::string gpioMarker[maxgpio][2]; // markers 1 M[n][0] for falling, M[n][1]
                                    // for rising inputs
bool gpioMarkerEnabled[maxgpio][2]; // markers 1 M[n][0] for falling, M[n][1]
                                    // for rising inputs

int gpioMarker_set(int channel, int direction, const string &value) {
    if (channel < 0 || channel >= ngpio)
        return -1;
    if (direction < 0 || direction > 1)
        return -1;
    gpioMarker[channel][direction] = value;
    return 0;
}

int gpioMarker_enable(int channel, int direction, bool enable = true) {
    if (channel < 0 || channel >= ngpio)
        return -1;
    if (direction < 0 || direction > 1)
        return -1;
    gpioMarkerEnabled[channel][direction] = enable;
    return 0;
}

int gpioMarker_disable(int channel, int direction) {
    return gpioMarker_enable(channel, direction, false);
}

void gpioMarkers_init_otocontrol() {
    gpioMarker_set(0, 0, "LEFT OFF");
    gpioMarker_set(0, 1, "LEFT ON");
    gpioMarker_enable(0, 0, true);
    gpioMarker_enable(0, 1, true);

    gpioMarker_set(1, 0, "RIGHT OFF");
    gpioMarker_set(1, 1, "RIGHT ON");
    gpioMarker_enable(1, 0, true);
    gpioMarker_enable(1, 1, true);

    gpioMarker_set(2, 0, "BTN RELEASE");
    gpioMarker_set(2, 1, "BTN PRESS");
    gpioMarker_enable(2, 0, true);
    gpioMarker_enable(2, 1, true);

    gpioMarker_set(3, 0, "BNC OFF");
    gpioMarker_set(3, 1, "BNC ON");
    gpioMarker_enable(3, 0, true);
    gpioMarker_enable(3, 1, true);
}

void gpioMarkers_init() {
    for (int i = 0; i < ngpio; i++) {
        ostringstream s0;
        s0 << i << " FALL";
        gpioMarker[i][0] = s0.str();
    }
    for (int i = 0; i < ngpio; i++) {
        ostringstream s1;
        s1 << i << " RISE";
        gpioMarker[i][1] = s1.str();
    }
    for (int i = 0; i < ngpio; i++) {
        gpioMarkerEnabled[i][0] = false;
        gpioMarkerEnabled[i][1] = true;
    }
    switch (lslder_config) {
    case LSLDER_OTOCONTROL:
    case LSLDER_OTOCONTROL_v1:
    case LSLDER_OTOCONTROL_v4:
        gpioMarkers_init_otocontrol();
        break;
    default:
        break;
    }
}

int ledtime = 8; // in 1/100 sec., see infinite loop in main()
int ledtimer[maxgpio] = {
    0,
};
int ledstatus[maxgpio] = {
    0,
};

int debouncetimes[maxgpio] = {
    0,
};

int set_debouncetime(int gpio, int millisecs) {
    if (gpio < 0 || gpio >= ngpio)
        return -1;
    if (millisecs < 0 || millisecs > 1000)
        return -1;
    return debouncetimes[gpio] = int(millisecs / 10);
}

void debouncetimes_init() {
    for (int i = 0; i < ngpio; i++)
        debouncetimes[i] = default_debouncetime;
}

int debouncetimer[maxgpio] = {
    0,
};

// for debugging:
int interrupt_count[maxgpio] = {
    0,
};

stream_outlet *numeric_outlet[maxgpio];
stream_outlet *marker_outlet;

std::mutex lsl_mutex;
typedef char int8;

bool run = false;

void stop(int signum) { run = false; }

class qelement {
  public:
    double timestamp;
    int bnc_input;
    int edge;

    qelement() {
        timestamp = -1;
        bnc_input = -1;
        edge = -1;
    }

    qelement(double _timestamp, int _bnc_input, int _edge) {
        timestamp = _timestamp;
        bnc_input = _bnc_input;
        edge = _edge;
    }
};

queue<qelement> lsl_event_queue(10000);
queue<qelement> zmq_event_queue(10000);

void gpioISR(int gpio, int level, uint32_t tick) {
    uint32_t now = gpioTick();
    uint32_t offset = now - tick;
    double timestamp = lsl_local_clock() - double(offset) / 1e6;

    if (!run)
        return;

    if (level == 2) // timeout call
        return;

    int bnc_input = gpiomap[gpio];
    if (gpioinp_invert[gpio])
      level = 1 - level;
    int edge = level;

    if (debouncetimer[bnc_input])
        return;
    else
        debouncetimer[bnc_input] = debouncetimes[bnc_input];

    if (debug) {
        std::lock_guard<std::mutex> guard(lsl_mutex);
        std::cout << "Interrupt on bnc_input " << bnc_input
                  << " level: " << level << " (GPIO " << gpio
                  << ")" << std::endl;
    }

    if (edge == 0 || edge == 1) {
        if (gpioMarkerEnabled[bnc_input][edge]) {
           std::lock_guard<std::mutex> guard(lsl_mutex);
           if (!lsl_event_queue.isFull())
               lsl_event_queue.enqueue(qelement(timestamp, bnc_input, level));
           else {
               // signalize queue overflow
               for (int i = 0; (i < ngpio) && (i < nled); i++)
                   ledtimer[i] = 50; // long blink all leds
           }
        }
    }

    if (edge == 1) { // old style behavior
        ledtimer[bnc_input] = ledtime;
    }

    if (edge == 0 || edge == 1) {
        if (gpioMarkerEnabled[bnc_input][edge]) {
            ledtimer[bnc_input] = ledtime;
        }
    }

    if (debug) {
        std::lock_guard<std::mutex> guard(lsl_mutex);
        interrupt_count[bnc_input]++;
        std::cout << bnc_input << " (count=" << interrupt_count[bnc_input]
                  << ")" << std::endl;
    }
}

void handleLeds() {
    for (int i = 0; (i < ngpio) && (i < nled); i++) {
        if (ledtimer[i] > 0) {
            if (!ledstatus[i]) {
                setled(i, 1);
                ledstatus[i] = 1;
            }
            ledtimer[i]--;
        }
        else {
            if (ledstatus[i]) {
                setled(i, 0);
                ledstatus[i] = 0;
            }
        }
    }
}

int rtpriority(int n) {
    int r = mlockall(MCL_CURRENT | MCL_FUTURE | MCL_ONFAULT);
    if (r < 0)
        return r;

    struct sched_param sched;

    memset(&sched, 0, sizeof(sched));

    if (n > sched_get_priority_max(SCHED_FIFO))
        sched.sched_priority = sched_get_priority_max(SCHED_FIFO);
    else
        sched.sched_priority = n;

    return sched_setscheduler(0, SCHED_FIFO, &sched);
}

#if 0
int zmq_recv_multipart(zmq::socket_t &socket, zmq::message_t parts[], int nmax) {
    int64_t more;
    size_t more_size = sizeof more;
    int n = 0;
    try {
        do {
            if (n < nmax) {
                socket.recv(parts[n]);
                if (verbose)
                    cout << "recv part " << n << endl;
                n++;
            }
            else {
                zmq::message_t discard;
                socket.recv(discard);
                if (verbose)
                    cout << "recv part " << n << " (discarded)." << endl;
            }
            socket.getsockopt(ZMQ_RCVMORE, &more, &more_size);
        } while (more);
    }
    catch (std::exception &error) {
        cout << "zmq_rev_multipart while receiving frame " << n << ": "
             << error.what() << endl;
    }
    return n;
}
#endif

int main(int argc, const char *argv[]) {
    parse_options(argc, argv);
    init_gpio_config(lslder_config);
    init_gpiomap();
    gpioMarkers_init();
    debouncetimes_init();

#ifdef IGNACIO
#warning Special for Ignacio
    gpioMarker_set(0, 1, "STIM");
    gpioMarker_set(1, 1, "STD");
    gpioMarker_set(2, 1, "DEV");
    gpioMarker_enable(3, 0, true);
    gpioMarker_enable(3, 1, false);
    gpioMarker_set(3, 0, "BTN");
#endif

    if (rtpriority(40) < 0) {
        perror("rtpriority");
        return 1;
    }

    // configure input sample rate
    int r = gpioCfgClock(5, 1, 0); // User 5 us sample rate on GPIO pins
    if (r < 0) {
        perror("gpioCfgClock");
        return 1;
    }

    // configure input channels
    r = gpioInitialise(); // use Broadcom pin numbering
    if (r < 0) {
        perror("gpioInitialise");
        return 1;
    }

    gpioSetSignalFunc(SIGINT, stop);
    gpioSetSignalFunc(SIGABRT, stop);
    gpioSetSignalFunc(SIGTERM, stop);

    printf("Press control C to stop.\n");

    initleds(nled, ledgpio);

    char hostname[255];
    gethostname(hostname, 255);
    for (int i = 0; i < ngpio; i++) {
        // make a new stream_info and open an outlet with it
        char info_name_digital[255];
        sprintf(info_name_digital, "Digital Events %d", i);

        char info_type_digital[255 + 18];
        sprintf(info_type_digital, "Digital Events @ %s", hostname);

        stream_info info(info_name_digital, info_type_digital, 1,
                         lsl::IRREGULAR_RATE, lsl::cf_int8,
                         "Raspberry Pi Digital Event Recorder");
        numeric_outlet[i] = new stream_outlet(info);
    }

    // make a new stream_info and open an outlet with it
    stream_info info_markers(info_name_markers.c_str(),
                             info_type_markers.c_str(), 1, lsl::IRREGULAR_RATE,
                             lsl::cf_string, info_sourceid_markers.c_str());
    marker_outlet = new stream_outlet(info_markers);

    for (int i = 0; i < ngpio; i++) {
        int bcmgpio = gpioinp[i]; // BCM GPIO number
        if (gpioSetMode(bcmgpio, PI_INPUT) < 0) {
            perror("gpioSetMode");
            return 1;
         }

        // The 4N35 optocoupler in the input circuit inverts the signal.
        // To detect rising edges in the input signal look at the falling input,
        // and the other way around.
        if (gpioSetAlertFunc(bcmgpio, gpioISR) < 0) {
            perror("gpioSetISRFunc");
            return 1;
        }
        if (gpioSetPullUpDown(bcmgpio, PI_PUD_UP) < 0) {
            perror("gpioSetMode");
            return 1;
        }

        if (debug)
            std::cout << "Set IRQ handler for gpio " << bcmgpio << std::endl;
    }

    //  Prepare our context and socket
    zmq::context_t ctx;
    zmq::socket_t cmd_sock(ctx, zmq::socket_type::rep);
    cmd_sock.bind("tcp://*:5555");

    zmq::socket_t data_sock(ctx, zmq::socket_type::pub);
    data_sock.bind("tcp://*:5556");

    const std::chrono::milliseconds pollin_timeout{0};
    zmq::pollitem_t pollitem{static_cast<void *>(cmd_sock), 0, ZMQ_POLLIN, 0};

    run = true;
    while (run) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        handleLeds();
        for (int i = 0; i < ngpio; i++) {
            if (debouncetimer[i] > 0)
                debouncetimer[i]--;
        }

        while (sd_serial && sd_serial->avail())
        {
           char buf[255];
           sd_serial->readline(buf, 255);
           //cout << "esp32: " << buf << endl;
        }


        while (!lsl_event_queue.isEmpty()) {
            std::lock_guard<std::mutex> guard(lsl_mutex);
            qelement el = lsl_event_queue.peek();
            lsl_event_queue.dequeue();

            // copy the data to the zmq event queue
            if (!zmq_event_queue.isFull())
                zmq_event_queue.enqueue(el);

            if (el.edge == 1) {
                // 'old style behaviour: only report rising edges (i.e. level ==
                // 0, inverted inputs!)
                numeric_outlet[el.bnc_input]->push_sample(&el.edge,
                                                          el.timestamp);
            }

                marker_outlet->push_sample(&gpioMarker[el.bnc_input][el.edge],
                                           el.timestamp);
                try {
                    auto r = data_sock.send(zmq::str_buffer("marker"),
                                            zmq::send_flags::sndmore);
                    const double timestamp = el.timestamp;
                    r = data_sock.send(
                        zmq::buffer(gpioMarker[el.bnc_input][el.edge]),
                        zmq::send_flags::sndmore);
                    r = data_sock.send(
                        zmq::const_buffer(&timestamp, sizeof(double)),
                        zmq::send_flags::none);
                }
                catch (zmq::error_t &e) {
                    std::cerr << std::endl
                              << "error: " << e.what() << '\n'
                              << "exception nr: " << e.num() << std::endl;
                }
        }

        zmq::poll(&pollitem, 1, pollin_timeout);
        if ((pollitem.revents & ZMQ_POLLIN)) {
            // read command from socket
            std::vector<zmq::message_t> recv_msgs;
            const auto ret =
                zmq::recv_multipart(cmd_sock, std::back_inserter(recv_msgs));
            if (!ret)
                return 1;
            if (verbose)
                std::cout << "Got " << *ret << " messages" << std::endl;

            // convert zmq messages to strings
            std::vector<std::string> recv_strings;
            for (auto p = recv_msgs.begin(); p != recv_msgs.end(); p++) {
                recv_strings.push_back(p->to_string());
            }

            json j;
            json jresult;
            try {
                for (auto p = recv_strings.begin(); p != recv_strings.end();
                     p++) {
                    std::string s = *p;
                    if (verbose)
                        std::cout << "message: " << s << std::endl;
                    j = json::parse(s);
                    jresult["result"] = "ok"; // assume success for now
                    jresult["line"] = j;
                    jresult["what"] = "";

                    if (verbose)
                        for (auto jp = j.begin(); jp != j.end(); jp++)
                            std::cout << "part: " << *jp << std::endl;
                    std::string cmd = j["cmd"].get<std::string>();
                    if (verbose)
                        std::cout << "cmd: " << cmd << std::endl;

                    if (cmd == "marker") {
                        std::string cmd = j["cmd"].get<std::string>();
                        int chan = j["chan"].get<int>();
                        int edge = j["edge"].get<int>();
                        std::string label = j["label"].get<std::string>();
                        if (gpioMarker_set(chan, edge, label) < 0) {
                            jresult["result"] = "error";
                            jresult["what"] = "value out of range";
                        }
                    }
                    else if (cmd == "enable") {
                        int chan = j["chan"].get<int>();
                        int edge = j["edge"].get<int>();
                        int enable = j["enable"].get<int>();
                        if (gpioMarker_enable(chan, edge, enable) < 0) {
                            jresult["result"] = "error";
                            jresult["what"] = "value out of range";
                        }
                    }
                    else if (cmd == "debounce") {
                        int chan = j["chan"].get<int>();
                        int time = j["time"].get<int>();
                        if (set_debouncetime(chan, time) < 0) {
                            jresult["result"] = "error";
                            jresult["what"] = "value out of range";
                        }
                    }
                    else if (cmd == "clear") {
                        std::lock_guard<std::mutex> guard(
                            lsl_mutex); // only needed for lsl_event_queue
                        lsl_event_queue.clear();
                        zmq_event_queue.clear();
                    }
                    else if (cmd == "get") {
                        json jel;
                        if (!zmq_event_queue.isEmpty()) {
                            qelement el = zmq_event_queue.peek();
                            zmq_event_queue.dequeue();
                            jel["chan"] = el.bnc_input;
                            jel["edge"] = el.edge;
                            jel["marker"] = gpioMarker[el.bnc_input][el.edge];
                            jel["timestamp"] = el.timestamp;
                        }
                        jresult["event"] = jel;
                    }
                    else if (cmd == "read") {
                        json jarr = json::array();
                        while (!zmq_event_queue.isEmpty()) {
                            json jel;
                            qelement el = zmq_event_queue.peek();
                            zmq_event_queue.dequeue();
                            jel["chan"] = el.bnc_input;
                            jel["edge"] = el.edge;
                            jel["marker"] = gpioMarker[el.bnc_input][el.edge];
                            jel["timestamp"] = el.timestamp;
                            jarr.push_back(jel);
                        }
                        jresult["events"] = jarr;
                    }
                    else if (cmd == "poll") {
                        jresult["size"] = zmq_event_queue.size();
                    }
                    else if (cmd == "reset") {
                        gpioMarkers_init();
                        debouncetimes_init();
                    }
                    else if (cmd == "clock") {
                        std::lock_guard<std::mutex> guard(lsl_mutex);
                        jresult["timestamp"] = lsl_local_clock();
                    }
                    else if (cmd ==
                             "sdgetst") { // get esp32 sound detector stats
                        if (sd_serial) {
                            int nsamp = j["nsamp"].get<int>();
                            sd_stats_t st;
                            char s[255];
                            snprintf(s, 255, "S %u", nsamp);
                            sd_serial->writeline(s);
                            char sret[255];
                            do {
                               sd_serial->readline(sret, 255);
                            } while (sret[0]=='#');
                            st.fromString(sret);
                            jresult["stats"] = {
                                {"nchan", sd_stats_t::nchan},
                                {"nsamp", st.nsamp},
                                {"offset", {st.offset[0], st.offset[1]}},
                                {"vrms", {st.vrms[0], st.vrms[1]}},
                                {"emax", {st.emax[0], st.emax[1]}},
                                {"avg_on", {st.avg_on[0], st.avg_on[1]}},
                                {"avg_off", {st.avg_off[0], st.avg_off[1]}},
                                {"avg_on_max",
                                 {st.avg_on_max[0], st.avg_on_max[1]}},
                                {"avg_off_max",
                                 {st.avg_off_max[0], st.avg_off_max[1]}},
                                {"signal_on",
                                 {st.signal_on[0], st.signal_on[1]}}};
                        }
                        else {
                            jresult["result"] = "error";
                            jresult["line"] = j;
                            jresult["what"] =
                                "no serial connection to sound detector";
                        }
                    }
                    else if (cmd == "sdgetcf") {
                        if (sd_serial) {
                            sd_config_t cf;
                            const char *s = "C";
                            sd_serial->writeline(s);
                            char sret[255];
                            do {
                               sd_serial->readline(sret, 255);
                            } while (sret[0]=='#');
                            cf.fromString(sret);
                            jresult["config"] = {
                                {"nchan", sd_config_t::nchan},
                                {"offset", {cf.offset[0], cf.offset[1]}},
                                {"auto_offset", {cf.auto_offset[0], cf.auto_offset[1]}},
                                {"debug", cf.debug},
                                {"analogFilterLen", cf.analogFilterLen},
                                {"Vthresh_lo", cf.Vthresh_lo},
                                {"Vthresh_hi", cf.Vthresh_hi},
                                {"nbuf_on", cf.nbuf_on},
                                {"nbuf_off", cf.nbuf_off},
                                {"debounce_samples", cf.debounce_samples},
                                {"peak_avg_on_thresh", cf.peak_avg_on_thresh},
                                {"peak_avg_off_thresh", cf.peak_avg_off_thresh}};
                        }
                        else {
                            jresult["result"] = "error";
                            jresult["line"] = j;
                            jresult["what"] =
                                "no serial connection to sound detector";
                        }
                    }
                    else if (cmd == "sdsetcf") {
                        if (sd_serial) {
                            sd_config_t cf;
                            auto v =
                                j["config"]["offset"].template get<std::vector<int>>();
                            cf.offset[0] = v[0];
                            cf.offset[1] = v[1];
                            auto v2 =
                                j["config"]["auto_offset"].template get<std::vector<int>>();
                            cf.auto_offset[0] = v2[0];
                            cf.auto_offset[1] = v2[1];
                            cf.debug = j["config"]["debug"];
                            cf.analogFilterLen = j["config"]["analogFilterLen"];
                            cf.Vthresh_lo = j["config"]["Vthresh_lo"];
                            cf.Vthresh_hi = j["config"]["Vthresh_hi"];
                            cf.nbuf_on = j["config"]["nbuf_on"];
                            cf.nbuf_off = j["config"]["nbuf_off"];
                            cf.debounce_samples = j["config"]["debounce_samples"];
                            cf.peak_avg_on_thresh = j["config"]["peak_avg_on_thresh"];
                            cf.peak_avg_off_thresh = j["config"]["peak_avg_off_thresh"];
                            char s[255];
                            cf.toString(s, 255);
                            sd_serial->writeline(s);
                            do {
                               sd_serial->readline(s, 255);
                            } while (s[0]=='#');
                            cf.fromString(s);
                            jresult["config"] = {
                                {"nchan", sd_config_t::nchan},
                                {"offset", {cf.offset[0], cf.offset[1]}},
                                {"auto_offset", {cf.auto_offset[0], cf.auto_offset[1]}},
                                {"debug", cf.debug},
                                {"analogFilterLen", cf.analogFilterLen},
                                {"Vthresh_lo", cf.Vthresh_lo},
                                {"Vthresh_hi", cf.Vthresh_hi},
                                {"nbuf_on", cf.nbuf_on},
                                {"nbuf_off", cf.nbuf_off},
                                {"debounce_samples", cf.debounce_samples},
                                {"peak_avg_on_thresh", cf.peak_avg_on_thresh},
                                {"peak_avg_off_thresh", cf.peak_avg_off_thresh}};
                        }
                        else {
                            jresult["result"] = "error";
                            jresult["line"] = j;
                            jresult["what"] =
                                "no serial connection to sound detector";
                        }
                    }
                    else if (cmd == "bootpm") {
                        if (sd_serial) {
                            const char *cmd = "P";
                            cout << "putting esp32 in boot program mode\n";
                            sd_serial->writeline(cmd);
                            char s[255];
                            do {
                               sd_serial->readline(s, 255);
                            } while (s[0]=='#');
                            cout << "esp: " << s << endl;
                            cout << "clearing serial connection\n";
                            delete sd_serial;
                            sd_serial = nullptr;
                            cout << "restart program and reset esp32 to start over (or power cycle)\n";
                        }
                        else {
                            jresult["result"] = "error";
                            jresult["line"] = j;
                            jresult["what"] =
                                "no serial connection to sound detector";
                        }
                    }
                    else if (cmd == "restart") {
                        if (sd_serial) {
                            const char *cmd = "R";
                            cout << "restarting esp32\n";
                            sd_serial->writeline(cmd);
                            char s[255];
                            do {
                               sd_serial->readline(s, 255);
                            } while (s[0]=='#');
                            cout << "esp: " << s << endl;
                        }
                        else {
                            jresult["result"] = "error";
                            jresult["line"] = j;
                            jresult["what"] =
                                "no serial connection to sound detector";
                        }
                    }
                    else {
                        jresult["result"] = "error";
                        jresult["line"] = j;
                        jresult["what"] = "unknown command";
                    }
                }
            }
            catch (json::exception &e) {
                std::cerr << std::endl
                          << "error: " << e.what() << '\n'
                          << "exception id: " << e.id << std::endl;
                jresult["result"] = "error";
                jresult["what"] = e.what();
            }
            zmq::message_t zresult(jresult.dump());
            cmd_sock.send(zresult, zmq::send_flags::none);
        }
    }
    gpioTerminate();
    return 0;
}
