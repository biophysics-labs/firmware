#ifndef SOUND_DETECTOR_H
#define SOUND_DETECTOR_H

#include <math.h>
#include <stdio.h>

struct sd_config_t {
  static constexpr int Vref = 3300;  // ADC reference voltage
  static constexpr int nchan = 2;
  static constexpr int maxAnalogFilterLen = 4;
  static constexpr int maxAnalogAvarageLen = 5000;
  static constexpr int maxNbufOff = 2000;
  static constexpr int maxNbufOn = 2000;

  int offset[2];  // ADC inputs offset
  int auto_offset[2];
  int debug;
  int analogFilterLen;
  int Vthresh_lo;
  int Vthresh_hi;
  int nbuf_on;
  int nbuf_off;
  int debounce_samples;
  float peak_avg_on_thresh;
  float peak_avg_off_thresh;
  sd_config_t() {
    debug = 0;
    reset();
  }
  void reset() {
    offset[0] = 0;
    offset[1] = 0;
    auto_offset[0] = 1;
    auto_offset[1] = 1;
    analogFilterLen = 4;
    Vthresh_lo = 6;  // the thresholds will eventually be set in the loop()
    Vthresh_hi = 6;  // by the reading of the potentiometer on the analogThreshPin
    nbuf_off = 800;
    nbuf_on = 40;
    debounce_samples = 1000;    // #samples to wait after level change was detected
    peak_avg_on_thresh = 0.10;
    peak_avg_off_thresh = 0.99;
  }
  int toString(char* s, int sz) {
    return snprintf(s, sz, "C %d %d %d %d %d %d %d %d %d %d %d %.2f %.2f",
                    offset[0],
                    offset[1],
                    auto_offset[0],
                    auto_offset[1],
                    debug,
                    analogFilterLen,
                    Vthresh_lo,
                    Vthresh_hi,
                    nbuf_on,
                    nbuf_off,
                    debounce_samples,
                    peak_avg_on_thresh,
                    peak_avg_off_thresh);
  }
  int fromString(const char* s) {
    char cmd;
    sscanf(s, "%c %d %d %d %d %d %d %d %d %d %d %d %f %f",
           &cmd,
           &offset[0],
           &offset[1],
           &auto_offset[0],
           &auto_offset[1],
           &debug,
           &analogFilterLen,
           &Vthresh_lo,
           &Vthresh_hi,
           &nbuf_on,
           &nbuf_off,
           &debounce_samples,
           &peak_avg_on_thresh,
           &peak_avg_off_thresh);
    if (analogFilterLen > maxAnalogFilterLen)
      analogFilterLen = maxAnalogFilterLen;
    if (analogFilterLen < 1)
      analogFilterLen = 1;
    if (nbuf_on > maxNbufOn)
      nbuf_on = maxNbufOn;
    if (nbuf_on < 1)
      nbuf_on = 1;
    if (nbuf_off > maxNbufOff)
      nbuf_off = maxNbufOff;
    if (nbuf_off < 1)
      nbuf_off = 1;
    if (cmd != 'C') {
      reset();
      return -1;
    }
    return 0;
  }
};



class sd_stats_t {
public:
  static constexpr int nchan = sd_config_t::nchan;
  static constexpr int bufsz = 10000;
  int nsamp;
  int offset[nchan];
  float vrms[nchan];
  int emax[nchan];
  float avg_on[nchan];
  float avg_off[nchan];
  float avg_on_max[nchan];
  float avg_off_max[nchan];
  int signal_on[nchan];

protected:
  int32_t buf[nchan][bufsz];
  int32_t raw_buf[nchan][bufsz];
  int head[nchan];
  int32_t sum[nchan];

public:
  sd_stats_t() {
    nsamp = 5000;
    for (int i = 0; i < nchan; i++) {
      for (int j = 0; j < bufsz; j++) {
         buf[i][j] = 0;
         raw_buf[i][j] = 0;
      }
      head[i] = 0;
      sum[i] = 0;
      //offset[i] = sd_config_t::Vref / 2;
      offset[i] = 0;
      signal_on[i] = 0;
      avg_on[i] = 0;
      avg_off[i] = 0;
      avg_on_max[i] = 0;
      avg_off_max[i] = 0;
    }
  }
  // called by analogReadChannelMav() to fill buffer on the fly
  void addSample(unsigned int chan, int32_t raw_value, int32_t filtered_value) {
    if (chan < nchan) {
      head[chan]++;
      if (head[chan] >= bufsz)
        head[chan] = 0;
      int tail = head[chan] - nsamp;
      if (tail < 0)
          tail += bufsz;

      raw_buf[chan][head[chan]] = raw_value;

      sum[chan] -= buf[chan][tail];
      buf[chan][head[chan]] = filtered_value;
      sum[chan] += filtered_value;
      offset[chan] = sum[chan]/nsamp;
    }
  }
  void compute(int _nsamp) {
    nsamp = _nsamp;
    if (nsamp > bufsz)
      nsamp = bufsz;
    for (int chan = 0; chan < 2; chan++) {
      int istart = head[chan] - nsamp;
      if (istart < 0)
        istart += bufsz;

      // compute max error realtive to offset
      emax[chan] = 0;
      for (int i = 0; i < nsamp; i++) {
        int d = buf[chan][(i + istart) % bufsz] - offset[chan];
        d = abs(d);
        if (d > emax[chan])
          emax[chan] = d;
      }

      // compute mean value of buffer
      long int sum = 0;
      int32_t mean;
      for (int i = 0; i < nsamp; i++) {
        sum += raw_buf[chan][(i + istart) % bufsz];
      }
      mean = sum / nsamp;

      // estimate rms voltage from buffer with raw values
      int32_t ssqr = 0;
      for (int i = 0; i < nsamp; i++) {
        int d = raw_buf[chan][(i + istart) % bufsz] - mean;
        ssqr += (d * d);
      }
      float vsqr = float(ssqr) / (nsamp); 
      vsqr /= 1e6; // convert from mV^2 to V^2
      vrms[chan] = sqrt(vsqr); // vrms in volts
    }
  }
  
  int toString(char* s, int sz) {
    return snprintf(s, sz, "S %d %d %d %.4f %.4f %d %d"
                           " %.2f %.2f %.2f %.2f"
                           " %.2f %.2f %.2f %.2f"
                           " %d %d",
                    nsamp, offset[0], offset[1], vrms[0], vrms[1], emax[0], emax[1],
                    avg_on[0], avg_on[1], avg_off[0], avg_off[1],
                    avg_on_max[0], avg_on_max[1], avg_off_max[0], avg_off_max[1],
                    signal_on[0], signal_on[1]);
  }
  int fromString(const char* s) {
    char cmd;
    sscanf(s, "%c %d %d %d %f %f %d %d"
           " %f %f %f %f"
          " %f %f %f %f"
          " %d %d",
          &cmd, &nsamp, &offset[0], &offset[1], &vrms[0], &vrms[1], &emax[0], &emax[1],
          &avg_on[0], &avg_on[1], &avg_off[0], &avg_off[1],
          &avg_on_max[0], &avg_on_max[1], &avg_off_max[0], &avg_off_max[1],
          &signal_on[0], &signal_on[1]);
    if (cmd != 'S') {
      return -1;
    }
    return 0;
  }
};

#endif // SOUND_DETECTOR_H
