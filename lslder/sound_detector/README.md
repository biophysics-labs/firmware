Install arduino-cli for Seeed Studio XIAO_ESP32C3 on raspberry pi

See also: https://arduino.github.io/arduino-cli/0.28/installation/

!!! Run raspi-config and enable the serial port hardware!!
$ sudo bash
# cd /usr/local
# curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | sh
# exit
$
installs arduino-cli in /usr/local/bin

You will need the python serial library to compile code
$ sudo apt install python3-serial

$ arduino-cli config init
See https://arduino.github.io/arduino-cli/0.28/commands/arduino-cli_config_init/

$ vi ~/.arduino15/arduino-cli.yaml
add https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json to  "additional_urls"
See https://wiki.seeedstudio.com/XIAO_ESP32C3_Getting_Started/#software-setup for the correct URL

----- arduino-cli.yaml  example ------
board_manager:
    additional_urls:
      - https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
--------------- END ------------------

$ arduino-cli core update-index
$ arduino-cli core install esp32:esp32

You can now search for the board, eg.
$ arduino-cli board search xiao
Board Name   FQBN                     Platform ID
XIAO_ESP32C3 esp32:esp32:XIAO_ESP32C3 esp32:esp32
XIAO_ESP32S3 esp32:esp32:XIAO_ESP32S3 esp32:esp32

Update 
$ arduino-cli lib update-index

List the board:  (the board is on ttyS0, WHY IS IT NOT LISTED?)
$ arduino-cli board list
Port       Protocol Type        Board Name FQBN Core
/dev/ttyS0 serial   Serial Port Unknown

create a new empty sketch:

$ arduino-cli sketch new blink
Sketch created in: /home/pi/blink

Edit the sketch
$ vi blink/blink.ino

and insert the following code:

int led = D10;

void setup() {
  // initialize digital pin led as an output
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);   // turn the LED on 
  delay(1000);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off
  delay(1000);               // wait for a second
}

Try to compile the code:
$ arduino-cli compile --fqbn esp32:esp32:XIAO_ESP32C3

We have the XIAO_ESP32C3 on /dev/ttyS0:
$ cd blink
$ arduino-cli board attach -p /dev/ttyS0 -b esp32:esp32:XIAO_ESP32C3
-> result is in ./sketch.yaml

$ arduino-cli upload
esptool.py v4.5.1
Serial port /dev/ttyS0
Connecting...
Failed to get PID of a device on /dev/ttyS0, using standard reset sequence.

Chip is ESP32-C3 (revision v0.4)
Features: WiFi, BLE
Crystal is 40MHz
MAC: 54:32:04:89:1c:10
Uploading stub...
Running stub...
Stub running...
Changing baud rate to 921600
Changed.
Configuring flash size...
Flash will be erased from 0x00000000 to 0x00003fff...
Flash will be erased from 0x00008000 to 0x00008fff...
Flash will be erased from 0x0000e000 to 0x0000ffff...
Flash will be erased from 0x00010000 to 0x00048fff...
Compressed 13248 bytes to 9562...
Writing at 0x00000000... (100 %)
Wrote 13248 bytes (9562 compressed) at 0x00000000 in 0.4 seconds (effective 279.5 kbit/s)...
Hash of data verified.
Compressed 3072 bytes to 146...
Writing at 0x00008000... (100 %)
Wrote 3072 bytes (146 compressed) at 0x00008000 in 0.1 seconds (effective 401.9 kbit/s)...
Hash of data verified.
Compressed 8192 bytes to 47...
Writing at 0x0000e000... (100 %)
Wrote 8192 bytes (47 compressed) at 0x0000e000 in 0.1 seconds (effective 529.5 kbit/s)...
Hash of data verified.
Compressed 230256 bytes to 129140...
Writing at 0x00010000... (12 %)
Writing at 0x0001aa25... (25 %)
Writing at 0x00021c59... (37 %)
Writing at 0x00027b72... (50 %)
Writing at 0x0002e17a... (62 %)
Writing at 0x00033f78... (75 %)
Writing at 0x0003bbd8... (87 %)
Writing at 0x000428c8... (100 %)
Wrote 230256 bytes (129140 compressed) at 0x00010000 in 3.7 seconds (effective 500.4 kbit/s)...
Hash of data verified.

Leaving...
Hard resetting via RTS pin...
New upload port: /dev/ttyS0 (serial)

-> If it doesn't work, you may have to get the chip into boot loader mode, by holding the B-button while pressing the R-button, and then releasing it.


