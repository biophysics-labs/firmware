//
// sound detector for the OtoControl version of the LSL Digital Event Recorder
// This runs on a separate Seeed Studio XIAO_ESP32C3 module
// Sample rate with 3 analog reads and 3 digital outs per loop is about 3700 S/s
//
// v6: add moving average filter to analog inputs
//     change digitalPin[0] to D8, because esp won't boot when D9 is held high on boot (see ESP32C3 datasheet about "Strapping Pins")
// v7: add configurable detection parameters
#include <string>
#include <HardwareSerial.h>
#include "sound_detector.h"
#include "esp_system.h"
#include "soc/rtc_cntl_reg.h"

#define DEBUG 1
//#define SERIAL_MON       // Important: undef this for the production version!!!
#define BOARD_VERSION 4  // see number in drawing

#if BOARD_VERSION < 2                       // first prototype
const int analogPin[] = { A2, A1 };    // Tip(L) - Ring(R)
const int digitalPin[] = { D10, D7 };  // L - R
const int digitalpulse = D5;
#else
const int analogPin[] = { A1, A2 };    // Tip(L) - Ring(R)
const int digitalPin[] = { D8, D10 };  // L - R
const int digitalpulse = D9;
#endif


HardwareSerial MySerial0(0);
sd_config_t cf;
sd_stats_t stats;

float running_avg_on_max[sd_config_t::nchan];
float running_avg_off_max[sd_config_t::nchan];

inline void dprintln(const char* debuginfo)
{
  if (cf.debug)
     MySerial0.printf("#%s\n", debuginfo);
}

//
// Software triggering download mode for ESP32 S3/C3
//
void enterDownloadBoot(void)
{
    REG_WRITE(RTC_CNTL_OPTION1_REG, RTC_CNTL_FORCE_DOWNLOAD_BOOT);
    esp_restart();
}

void measureAnalogStats(int chan, int& nsamp, int& offset, int& erms, int& emax);
void measureAnalogStats(int chan, int& offset, int& erms, int& emax) {
  int nsamp = 5000;
  measureAnalogStats(chan, nsamp, offset, erms, emax);
};

static const int nchan = sd_config_t::nchan;

void processSerialCmd() {
  String str = MySerial0.readStringUntil('\n');
  const char* cmd = str.c_str();
  dprintln(cmd);
  char ret[255];
  switch (cmd[0]) {
    case 'P': 
       MySerial0.println("P Restarting and entering download boot mode");
       delay(1000); // give host program a chance to hang up serial connection
       enterDownloadBoot();
       break;

    case 'R': 
       MySerial0.println("R Restarting ESP");
       delay(1000); // give host program a chance to hang up serial connection
       esp_restart();
       break;

    case 'S':  // return the stats of the analog inputs
      {
        int nsamp = 5000; 
        char c;
        int nconv = sscanf(cmd, "%c %d", &c, &nsamp);
        if (nconv != 2) {
          snprintf(ret, sizeof(ret), "E esp32: parse error at: %s", cmd);
          MySerial0.println(ret);
          return;
        }
        stats.compute(nsamp);
        stats.toString(ret, sizeof(ret));
        for (int chan=0; chan<nchan; chan++) {
           stats.avg_on_max[chan] = 0;
           stats.avg_off_max[chan] = 0;
        }
        MySerial0.println(ret);
      }
      break;

    case 'C':
      if (cmd[1] == '\0') {  // return current config
        cf.toString(ret, sizeof(ret));
        MySerial0.println(ret);
      } else {  // set current config and return result
        cf.fromString(cmd);
        cf.toString(ret, sizeof(ret));
        MySerial0.println(ret);
      }
      break;

    default:
      snprintf(ret, sizeof(ret), "E esp32: unknown command: %s", cmd);
      MySerial0.println(ret);
      break;
  }
}

// variables for the ADC moving average filter
int32_t analogFilterBuf[nchan][sd_config_t::maxAnalogFilterLen];
int analogFilterPos[nchan] = {
  0,
};
int analogFilterSum[nchan] = {
  0,
};


int32_t analogAverageBuf[nchan][sd_config_t::maxAnalogAvarageLen];
int analogAveragePos[nchan] = {
  0,
};
int32_t analogAverageSum[nchan] = {
  0,
};
int32_t analogAverage[nchan] = {
  0,
};


int32_t analogReadChannelMav(uint8_t chan) {
  int32_t raw_val = analogReadMilliVolts(analogPin[chan]);
  raw_val -= (sd_config_t::Vref/2);
  int pos = analogFilterPos[chan];
  analogFilterSum[chan] -= analogFilterBuf[chan][pos];
  analogFilterSum[chan] += raw_val;
  analogFilterBuf[chan][pos] = raw_val;
  analogFilterPos[chan] = (pos + 1) % cf.analogFilterLen;
  int32_t filtered_val = analogFilterSum[chan] / cf.analogFilterLen;
  stats.addSample(chan, raw_val, filtered_val);
  if (cf.auto_offset[chan])
     cf.offset[chan] = stats.offset[chan];
  return filtered_val;
}

//
// constants and buffers for rectified peak detection
//

bool buf_off[nchan][sd_config_t::maxNbufOff] = {
  false,
};
bool buf_on[nchan][sd_config_t::maxNbufOn] = {
  false,
};
int32_t sum_off[nchan] = {
  0,
};
int32_t sum_on[nchan] = {
  0,
};

//
// constants and buffers for power based detection
//
int32_t sqr_off[nchan][sd_config_t::maxNbufOff] = {
  0,
};
int32_t sqr_on[nchan][sd_config_t::maxNbufOn] = {
  0,
};
int32_t sqrsum_off[nchan] = {
  0,
};
int32_t sqrsum_on[nchan] = {
  0,
};


//int offset[nchan];
int erms[nchan];
int emax[nchan];

int _pulse = 0;
void toggleDigitalPulse() {
  _pulse = 1 - _pulse;
  digitalWrite(digitalpulse, _pulse);
}

void setup() {

cf.debug = DEBUG;

#ifdef SERIAL_MON
  Serial.begin(921600);  //  setup serial
  while (!Serial) {}
  Serial.println("startup...");
#endif

//#ifndef SERIAL_MON  // don't wait long when debugging
////  delay(22000);     // this is the boot time of the raspberry
////#else
////  delay(2000);  // let input capacitors settle
////#endif

  delay(2000);  // let input capacitors settle

  // Configure MySerial0 on pins TX=6 and RX=7 (-1, -1 means use the default)
  MySerial0.begin(115200, SERIAL_8N1, -1, -1);  // This works for SEEED XIAO_ESP32C3 connection on D6 and D7
  MySerial0.setTimeout(50);
  MySerial0.println("\nESP32 Sound Detector Setup");

  pinMode(digitalpulse, OUTPUT);
  int32_t measuredNoise = 0;
  for (int i = 0; i < nchan; i++) {
    pinMode(digitalPin[i], OUTPUT);
    pinMode(analogPin[i], INPUT);
    int tries = 10;
    do {
      delay(200);
      toggleDigitalPulse();
      digitalWrite(digitalPin[i], HIGH);
      measureAnalogStats(i, cf.offset[i], erms[i], emax[i]);
      digitalWrite(digitalPin[i], LOW);
      delay(200);
      toggleDigitalPulse();
    } while ((erms[i] > 10) && (tries-- > 0));
    if (erms[i] > measuredNoise)
      measuredNoise = erms[i];
  }
  //digitalWrite(digitalPin[0], LOW);
  //digitalWrite(digitalPin[1], LOW);
}



int i_off = 0;
int i_on = 0;
//const int debounce_samples = 1000;
int debounce_count[nchan] = {
  0,
};
const int idle_print_interval = 4000;
int idle_print_count[nchan] = {
  0,
};

void loop() {
  if (MySerial0.available())
    processSerialCmd();

  toggleDigitalPulse();
  for (int chan = 0; chan < nchan; chan++) {
    int32_t Vin = analogReadChannelMav(chan);  // read the input pin
    Vin = Vin - int32_t(cf.offset[chan]);
    //
    // strategy: based on rectified voltages above thresholds
    //
    int32_t Vin_abs = abs(Vin);
    bool above_lo = Vin_abs > cf.Vthresh_lo;
    sum_on[chan] -= buf_on[chan][i_on];
    buf_on[chan][i_on] = above_lo;
    sum_on[chan] += above_lo;

    bool below_hi = Vin_abs < cf.Vthresh_hi;
    sum_off[chan] -= buf_off[chan][i_off];
    buf_off[chan][i_off] = below_hi;
    sum_off[chan] += below_hi;

    stats.avg_on[chan] = float(sum_on[chan]) / cf.nbuf_on;
    stats.avg_off[chan] = float(sum_off[chan]) / cf.nbuf_off;

#if 0
    if (idle_print_count[chan] > 0) {
      idle_print_count[chan]--;
      if (stats.avg_on[chan] > stats.avg_on_max[chan])
        stats.avg_on_max[chan] = stats.avg_on[chan];
    } else {
      idle_print_count[chan] = idle_print_interval;
      char buf[255];
      snprintf(buf, 255, "%i IDLE(%s) avg_on=%.2f (max=%.2f) avg_off=%.2f\t\t",
         chan, stats.signal_on[chan] ? "ON " : "OFF",
         stats.avg_on[chan], stats.avg_on_max[chan], stats.avg_off[chan]);
      dprintln(buf);
      stats.avg_on_max[chan] = 0;
    }
#endif

    if (stats.avg_on[chan] > running_avg_on_max[chan])
        running_avg_on_max[chan] = stats.avg_on[chan];
    if (stats.avg_off[chan] > running_avg_off_max[chan])
        running_avg_off_max[chan] = stats.avg_off[chan];

    if (debounce_count[chan] > 0) {
      debounce_count[chan]--;
    } else {
      if (stats.avg_on[chan] > cf.peak_avg_on_thresh) {
        if (!stats.signal_on[chan]) {  // change LO->HI
          // update level stats
          stats.signal_on[chan] = true;
          // update maximum avg_off found until this LO->HI change in stats
          stats.avg_off_max[chan] = running_avg_off_max[chan];
          running_avg_off_max[chan] = 0;
          // reset debounce count
          debounce_count[chan] = cf.debounce_samples;
          char buf[255];
            snprintf(buf, 255, "%i ON    avg_on=%.2f", chan, stats.avg_on[chan]);
          dprintln(buf);
            snprintf(buf, 255, "%i  avg_off_max=%.2f", chan, stats.avg_off_max[chan]);
          dprintln(buf);
        }
      } else {
        if (stats.avg_off[chan] >= cf.peak_avg_off_thresh) {
          if (stats.signal_on[chan]) { // change HI->LO
            // update level stats
            stats.signal_on[chan] = false;
            // update maximum avg_on found until this HI->LO change in stats
            stats.avg_on_max[chan] = running_avg_on_max[chan];
            running_avg_on_max[chan] = 0;
            // reset debounce count
            debounce_count[chan] = cf.debounce_samples;
            char buf[255];
            snprintf(buf, 255, "%i OFF  avg_off=%.2f", chan, stats.avg_off[chan]);
            dprintln(buf);
            snprintf(buf, 255, "%i   avg_on_max=%.2f", chan, stats.avg_on_max[chan]);
            dprintln(buf);
          }
        }
      }
    }
    //
    // digital output of result
    //
    // always do the digitalWrite(), to keep the timing of the loop uniform
    if (stats.signal_on[chan])
      digitalWrite(digitalPin[chan], HIGH);
    else
      digitalWrite(digitalPin[chan], LOW); 
  }
  i_on = (i_on + 1) % cf.nbuf_on;
  i_off = (i_off + 1) % cf.nbuf_off;
}


void measureAnalogStats(int chan, int& nsamp, int& offset, int& erms, int& emax) {
  const int nsamp_max = 10000;
  static int val[nsamp_max];  // must be static, because of limited stack size
  if (nsamp > nsamp_max)
    nsamp = nsamp_max;
  long int sum = 0;
  for (int i = 0; i < nsamp; i++) {
    val[i] = analogReadChannelMav(chan);
    sum += val[i];
  }

  offset = sum / nsamp;
  emax = 0;
  long int ssqr = 0;
  for (int i = 0; i < nsamp; i++) {
    int d = val[i] - offset;
    ssqr += d * d;
    d = abs(d);
    if (d > emax)
      emax = d;
  }
  int esqr = ssqr / nsamp;
  erms = sqrt(esqr);
#ifdef SERIAL_MON
  for (int i = 0; i < 100 && i < nsamp; i++) {
    Serial.printf("%i ", val[i] - offset);
  }
  Serial.println();
  Serial.printf("\tchan%i\tn=%i\toffset=%i\tesqr=%i\terms=%i\temax=%i\n", chan, nsamp, offset, esqr, erms, emax);
#endif
}
