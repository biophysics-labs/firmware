#include <lsl_cpp.h>
#include <stdlib.h>
#include <iostream>
#include <mutex>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <wiringPi.h>

using namespace lsl;

/**
 * This is an example of how a simple data stream can be offered on the network.
 * Here, the stream is named SimpleStream, has content-type EEG, and 8 channels.
 * The transmitted samples contain random numbers (and the sampling rate is irregular)
 */

stream_outlet *outlet;
std::mutex lsl_mutex;
typedef char int8;

void gpioInterrupt(const char* mrk)
{
    std::lock_guard<std::mutex> guard(lsl_mutex);
#ifdef DEBUG
    std::cout << mrk << std::endl;
#endif
    std::string sample(mrk);
    outlet->push_sample(&sample);
}

void gpioInterrupt_4()
{
    gpioInterrupt("EDGE_RISING_1"); // gpio4 has input label 1
}

void gpioInterrupt_17()
{
    gpioInterrupt("EDGE_RISING_2");
}

void gpioInterrupt_22()
{
    gpioInterrupt("EDGE_RISING_4");
}

void gpioInterrupt_27()
{
    gpioInterrupt("EDGE_RISING_3");
}

int main(int argc, char* argv[]) {
        piHiPri(99);
        //
        wiringPiSetupGpio();
        pinMode(4, INPUT);
        pinMode(17, INPUT);
        pinMode(22, INPUT);
        pinMode(27, INPUT);
        pullUpDnControl(4, PUD_UP);
        pullUpDnControl(17, PUD_UP);
        pullUpDnControl(22, PUD_UP);
        pullUpDnControl(27, PUD_UP);


        // make a new stream_info (nchannelsch) and open an outlet with it
        stream_info info("Digital Events", "Digital Events", 1, lsl::IRREGULAR_RATE,
           lsl::cf_string,"Raspberry Pi Digital Event Recorder");
        outlet = new stream_outlet(info);

        wiringPiISR(4, INT_EDGE_RISING, &gpioInterrupt_4);
        wiringPiISR(17, INT_EDGE_RISING, &gpioInterrupt_17);
        wiringPiISR(22, INT_EDGE_RISING, &gpioInterrupt_22);
        wiringPiISR(27, INT_EDGE_RISING, &gpioInterrupt_27);

        // send data forever
        while(true) 
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        return 0;
}

