#include "esp32serial.h"
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;

const char *dev = "/dev/ttyS0";
const int baud = 115200;

esp32serial *serial = 0;

int main()
{
   esp32serial serial(dev, baud);

      while (int n = serial.avail() > 0) {
         char buffer[512];
         int nread=serial.readline(buffer, sizeof(buffer));
         cout << buffer << endl;
      }
   int count = 0;
   while (true) {
      string s = "line number ";
      s += to_string(count);
      const char *buffer = s.c_str();
      const int length = s.length();
      cout << "writing \'" << buffer << "\'" << endl;
      serial.writeline(s.c_str(), length+1);
      while (serial.avail() <= 0) { };
      char sret[255];
      serial.readline(sret, 255);
      cout << "readback (" << strlen(sret) << "): \"" << sret << "\"" << endl;
      count++;
      //usleep(1000000);
   }
}
