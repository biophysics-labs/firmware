#include "led.h"
#include <pigpio.h>

#include <stdlib.h>
#include <unistd.h>
#include <iostream>

static int nled = 0;
static const int *ledgpio = 0;

void playleds();

void initleds(int _nled, const int *_ledgpio)
{
    nled = _nled;
    ledgpio = _ledgpio;
    for (int i=0; i<nled; i++)
    {
       gpioSetMode(ledgpio[i], PI_OUTPUT);
    }
    playleds();
}

void playleds()
{
    for (int i=0; i<nled; i++)
    {
       setled(i, 1);
       usleep(200000);
    }
    for (int i=0; i<nled; i++)
    {
       setled(i, 0);
       usleep(200000);
    }
    for (int i=0; i<nled; i++)
    {
       setled(i, 1);
    }
    usleep(500000);
    for (int i=0; i<nled; i++)
    {
       setled(i, 0);
    }
    usleep(500000);
    for (int i=0; i<nled; i++)
    {
       setled(i, 1);
    }
    usleep(500000);
    for (int i=0; i<nled; i++)
    {
       setled(i, 0);
    }
}


void setled(int lednr, int value)
{
   int bit;
   if (value)
      bit = 1;
   else
      bit = 0;
 
   int r= gpioWrite(ledgpio[lednr], bit);
   if (r < 0)
   {
      perror("gpioWrite");
      exit(1);
   }
}
