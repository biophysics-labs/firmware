//
// esp32serial -- class that communicates with the serial port UART on an esp32
//
#include <stdexcept> 
#include <termios.h>

class esp32serial {
   int fd;
   struct termios oldtty,tty;
public:
   bool verbose;
   esp32serial(const char *device, const int baudrate, bool _verbose = false);
   virtual ~esp32serial();
   int avail();
   int readline(char *result, const unsigned int size);
   int writeline(const char *buf);
   virtual speed_t baudrate2speed_t(const unsigned int baudrate);
};

