#include <iostream>
using namespace std;

int main()
{
   const char *s = "S 5000 1640 1642 0 1 2 3 0.04 0.05 1.06 1.07 0.08 0.09 0.10 0.11 12 13 ";

  static constexpr int nchan = 2;
  char cmd;
  unsigned int nsamp;
  unsigned int offset[nchan];
  int erms[nchan];
  int emax[nchan];
  float avg_on[nchan];
  float avg_off[nchan];
  float avg_on_max[nchan];
  float avg_off_max[nchan];
  int signal_on[nchan];
  int nconv = sscanf(s, "%c %d %d %d %d %d %d %d"
                          " %f %f %f %f"
                          " %f %f %f %f"
                          " %d %d",
                       &cmd, &nsamp, &offset[0], &offset[1], &erms[0], &erms[1], &emax[0], &emax[1],
                       &avg_on[0], &avg_on[1], &avg_off[0], &avg_off[1],
                       &avg_on_max[0], &avg_on_max[1], &avg_off_max[0], &avg_off_max[1],
                       &signal_on[0], &signal_on[1]);

   cout << "line=" << s << endl;
   cout << "nconv=" << nconv << endl;
   cout << "cmd=" << cmd << endl;
   cout << "nsamp=" << nsamp << endl;
   cout << "offset[0]=" << offset[0] << endl;
   cout << "offset[1]=" << offset[1] << endl;
   cout << "erms[0]=" << erms[0] << endl;
   cout << "erms[1]=" << erms[1] << endl;
   cout << "emax[0]=" << emax[0] << endl;
   cout << "emax[1]=" << emax[1] << endl;
   cout << "avg_on[0]=" << avg_on[0] << endl;
   cout << "avg_on[1]=" << avg_on[1] << endl;
   cout << "avg_off[0]=" << avg_off[0] << endl;
   cout << "avg_off[1]=" << avg_off[1] << endl;
   cout << "avg_on_max[0]=" << avg_on_max[0] << endl;
   cout << "avg_on_max[1]=" << avg_on_max[1] << endl;
   cout << "avg_off_max[0]=" << avg_off_max[0] << endl;
   cout << "avg_off_max[1]=" << avg_off_max[1] << endl;
   cout << "signal_on[0]=" << signal_on[0] << endl;
   cout << "signal_on[1]=" << signal_on[1] << endl;
}
