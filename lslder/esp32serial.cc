#include "esp32serial.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <string>

using namespace std;

esp32serial::esp32serial(const char *device, const int baudrate, bool _verbose) {
    verbose = _verbose;
    speed_t speed = baudrate2speed_t(baudrate);
    fd = open(device, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        string s = "esp32serial: open: ";
        s += device;
        s += ": ";
        s += strerror(errno);
        throw runtime_error(s);
    }
    tcgetattr(fd, &oldtty); // save current port settings
    tcgetattr(fd, &tty); 

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag |= CLOCAL | CREAD;
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    tty.c_lflag |= ICANON | ISIG;  /* canonical input */
    tty.c_lflag &= ~(ECHO | ECHOE | ECHONL | IEXTEN);

    tty.c_iflag |= IGNCR;  /* ignore carriage return */
    tty.c_iflag &= ~INPCK;
    tty.c_iflag &= ~(INLCR | ICRNL | IUCLC | IMAXBEL);
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);   /* no SW flowcontrol */

    tty.c_oflag &= ~OPOST;

    tty.c_cc[VEOL] = 0;
    tty.c_cc[VEOL2] = 0;
    tty.c_cc[VEOF] = 0x04;

    tcflush(fd, TCIFLUSH);
    tcsetattr(fd, TCSANOW, &tty);
}

int esp32serial::readline(char *result, const unsigned int size) {
    char buffer[255];  // Input buffer
    char *bufptr = buffer;
    int rdlen;
    // read characters into our string buffer until we get a NL
    while ((rdlen = ::read(fd, bufptr, buffer + sizeof(buffer) - bufptr - 1)) > 0) {
       bufptr += rdlen;
       if (bufptr[-1] == '\n') {
          bufptr[-1] = 0; // get rid of NL and null terminate
          break;
       }
    }
    // null terminate the string in case no NL was received
    bufptr[-1] = '\0';
    strncpy(result, buffer, size);
    if (verbose) {
       if (buffer[0] == '#')
          cout << "esp32serial (debug-read): \'" << result << "\'\n" << flush;
       else
          cout << "esp32serial: readline \'" << result << "\'\n" << flush;
    }
    return bufptr - buffer; // number of bytes read
}

int esp32serial::writeline(const char *buf) {
    int size = strlen(buf);
    int wlen = ::write(fd, buf, size);
    int wlen2 = ::write(fd, "\n", 1) ;
    if ((wlen != size) || (wlen2 != 1)) {
        string s = "esp32serial: write: ";
        s += strerror(errno);
        throw runtime_error(s);
    }
    tcdrain(fd); // wait until all characters have been transferred
    if (verbose)
       cout << "esp32serial: writeline \'" << buf << "\'\n" << flush;
    return wlen;
}

esp32serial::~esp32serial() {
    tcsetattr(fd, TCSANOW, &oldtty);
    close(fd);
}

int esp32serial::avail() {
    int result;
    if (ioctl(fd, FIONREAD, &result) == -1) {
        string s = "esp32serial: ioctl: ";
        s += strerror(errno);
        throw runtime_error(s);
    }
    return result;
}

speed_t esp32serial::baudrate2speed_t(const unsigned int baudrate) {
    switch (baudrate) {
    case 50:
        return B50;
    case 75:
        return B75;
    case 110:
        return B110;
    case 134:
        return B134;
    case 150:
        return B150;
    case 200:
        return B200;
    case 300:
        return B300;
    case 600:
        return B600;
    case 1200:
        return B1200;
    case 1800:
        return B1800;
    case 2400:
        return B2400;
    case 4800:
        return B4800;
    case 9600:
        return B9600;
    case 19200:
        return B19200;
    case 38400:
        return B38400;
    case 57600:
        return B57600;
    case 115200:
        return B115200;
    case 230400:
        return B230400;
    case 460800:
        return B460800;
    case 500000:
        return B500000;
    case 576000:
        return B576000;
    case 921600:
        return B921600;
    case 1000000:
        return B1000000;
    case 1152000:
        return B1152000;
    case 1500000:
        return B1500000;
    case 2000000:
        return B2000000;
    case 2500000:
        return B2500000;
    case 3000000:
        return B3000000;
    case 3500000:
        return B3500000;
    case 4000000:
        return B4000000;
    case B0:
    case B50:
    case B75:
    case B110:
    case B134:
    case B150:
    case B200:
    case B300:
    case B600:
    case B1200:
    case B1800:
    case B2400:
    case B4800:
    case B9600:
    case B19200:
    case B38400:
    case B57600:
    case B115200:
    case B230400:
    case B460800:
    case B500000:
    case B576000:
    case B921600:
    case B1000000:
    case B1152000:
    case B1500000:
    case B2000000:
    case B2500000:
    case B3000000:
    case B3500000:
    case B4000000:
        return baudrate;
    default:
        string s = string("esp32serial: wrong baudrate: ") + std::to_string(baudrate);
        throw runtime_error(s);
    }
}
