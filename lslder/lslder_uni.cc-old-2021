// lslder.cc -- GW/20200110
//
// LSL Digital Event Recorder
//
// This program offers a maximum of eight LSL event streams, each triggered by
// positive edges on raspberry pi GPIO digital inputs. Visual feedback is
// provided bij eight LEDs. The stream type is "Digital Events - $HOSTNAME",
// where HOSTNAME is the ip hostname of the raspberry. This is to uniquely
// identify devices on the network. The data sent with the events is an int8
// with value 1, with 1 indicating a rising edge. Falling edges can be sent with
// a 0 value. However this is not implemented in this program.
//
// Dependencies: labstreaminglayer C++

// CHANGES
// GW/20180716 initial version for 8-channel LSL Digital Event Recorder
// GW/20210210 remove deprecated wiringPi and use pigpio instead
// TODO: use ISR tick for debounce timing
// TODO: add support to register falling edges

#include "led.h"
#include <chrono> // std::chrono::seconds
#include <cstring>
#include <iostream>
#include <lsl_cpp.h>
#include <mutex>
#include <pigpio.h>
#include <sched.h>
#include <signal.h>
#include <stdlib.h>
#include <thread> // std::this_thread::sleep_for
#include <unistd.h>

//#define DEBUG

using namespace lsl;

#ifdef LSLDER_1CHAN

// 1 channel portable version without LEDs
const int ngpio = 1;
const int gpioinp[ngpio] = {8};

const int default_debouncetime = 100;

const int *ledgpio = 0;
const int nled = 0;

#endif

#ifdef LSLDER_4CHAN

// 4-chan version with 4 LEDs
const int ngpio = 4;
const int gpioinp[ngpio] = {15, 18, 23, 24};

const int default_debouncetime = 0;

const int ledgpio[] = {2, 3, 14, 5};
const int nled = 4;

#endif

#ifdef LSLDER_8CHAN

// 8-chan version with 8 LEDs
const int ngpio = 8;
const int gpioinp[ngpio] = {12, 7, 8, 25, 24, 23, 18, 15};

const int default_debouncetime = 0;

const int ledgpio[] = {21, 20, 16, 13, 5, 14, 3, 2};
const int nled = 8;

#endif

const int maxgpio = 54; // BCM chip has 54 GPIO
int gpiomap[maxgpio];

void init_gpiomap() {
    // map BNC input channel wiring 0..ngpio-1 to BCM GPIO
    for (int bnc_input = 0; bnc_input < ngpio; bnc_input++) {
        int bcmgpio = gpioinp[bnc_input];
        gpiomap[bcmgpio] = bnc_input;
    }
}

int ledtime = 2; // in 1/100 sec., see infinite loop in main()
volatile int ledtimer[ngpio] = {
    0,
};
int ledstatus[ngpio] = {
    0,
};

int debouncetimes[ngpio] = {
    0,
};
volatile int debouncetimer[ngpio] = {
    0,
};

#ifdef DEBUG
volatile int interrupt_count[ngpio] = {
    0,
};
#endif

stream_outlet *outlet[ngpio];
std::mutex lsl_mutex;
typedef char int8;

volatile int run = 1;

void stop(int signum) { run = 0; }

void gpioISR(int gpio, int level, uint32_t tick) {
    int bnc_input = gpiomap[gpio];

    if (level == 2) // timeout call
        return;

    if (debouncetimer[bnc_input])
        return;
    else
        debouncetimer[bnc_input] = debouncetimes[bnc_input];

    static int edge = 1;
    std::lock_guard<std::mutex> guard(lsl_mutex);
#ifdef DEBUG
    std::cout << "Interrupt on bnc_input " << bnc_input << " (GPIO "
              << gpioinp[bnc_input] << ")" << std::endl;
#endif
    outlet[bnc_input]->push_sample(&edge);
    ledtimer[bnc_input] = ledtime;

#ifdef DEBUG
    interrupt_count[bnc_input]++;
    std::cout << bnc_input << " (count=" << interrupt_count[bnc_input] << ")"
              << std::endl;
#endif
}

void handleLeds() {
    for (int i = 0; (i < ngpio) && (i < nled); i++) {
        if (ledtimer[i] > 0) {
            if (!ledstatus[i]) {
                setled(i, 1);
                ledstatus[i] = 1;
            }
            ledtimer[i]--;
        }
        else {
            if (ledstatus[i]) {
                setled(i, 0);
                ledstatus[i] = 0;
            }
        }
    }
}

int rtpriority(int n) {
    struct sched_param sched;

    memset(&sched, 0, sizeof(sched));

    if (n > sched_get_priority_max(SCHED_RR))
        sched.sched_priority = sched_get_priority_max(SCHED_RR);
    else
        sched.sched_priority = n;

    return sched_setscheduler(0, SCHED_RR, &sched);
}

int main(int argc, char *argv[]) {
    init_gpiomap();

    if (rtpriority(99) < 0) {
        perror("rtpriority");
        return 1;
    }

    // configure input channels
    int r = gpioInitialise(); // use Broadcom pin numbering
    if (r < 0) {
        perror("gpioInitialise");
        return 1;
    }

    gpioSetSignalFunc(SIGINT, stop);
    gpioSetSignalFunc(SIGABRT, stop);

    printf("Press control C to stop.\n");

    initleds(nled, ledgpio);

    for (int i = 0; i < ngpio; i++) {
        int bcmgpio = gpioinp[i]; // BCM GPIO number
        //if (gpioSetMode(bcmgpio, PI_INPUT) < 0) {
        //    perror("gpioSetMode");
        //    return 1;
        // }

        debouncetimes[i] = default_debouncetime;

        const int timeout_ms = 10000;
        // The 4N35 optocoupler in the input circuit inverts the signal, we want
        // to detect rising edges in the input signal, so here we program
        // FALLING_EDGE.
        if (gpioSetISRFunc(bcmgpio, FALLING_EDGE, timeout_ms, gpioISR) < 0) {
            perror("gpioSetISRFunc");
            return 1;
        }
        if (gpioSetPullUpDown(bcmgpio, PI_PUD_UP) < 0) {
            perror("gpioSetMode");
            return 1;
        }

#ifdef DEBUG
        std::cout << "Set IRQ handler for gpio " << bcmgpio << std::endl;
#endif

    }

    char hostname[255];
    gethostname(hostname, 255);
    for (int i = 0; i < ngpio; i++) {
        // make a new stream_info and open an outlet with it
        char info_name[255];
        sprintf(info_name, "Digital Events %d", i);

        char info_type[255 + 18];
        sprintf(info_type, "Digital Events @ %s", hostname);

        stream_info info(info_name, info_type, 1, lsl::IRREGULAR_RATE,
                         lsl::cf_int8, "Raspberry Pi Digital Event Recorder");
        outlet[i] = new stream_outlet(info);
    }

    // do this forever
    while (run) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        handleLeds();

        for (int i = 0; i < ngpio; i++) {
            if (debouncetimer[i] > 0)
                debouncetimer[i]--;
        }
    }
    gpioTerminate();
    return 0;
}
