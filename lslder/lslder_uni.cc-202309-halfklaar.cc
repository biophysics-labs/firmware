// lslder_uni.cc -- GW/20220310
//
// LSL Digital Event Recorder
//
// This program offers a maximum of eight LSL event streams, each triggered by
// positive edges on raspberry pi GPIO digital inputs. Visual feedback is
// provided bij eight LEDs. The stream type is "Digital Events - $HOSTNAME",
// where HOSTNAME is the ip hostname of the raspberry. This is to uniquely
// identify devices on the network. The data sent with the events is an int8
// with value 1, with 1 indicating a rising edge. Falling edges can be sent with
// a 0 value. However this is not implemented in this program.
//
// Dependencies: labstreaminglayer C++

// CHANGES
// GW/20180716 initial version for 8-channel LSL Digital Event Recorder
// GW/20210210 remove deprecated wiringPi and use pigpio instead
// TODO: use ISR tick for debounce timing
// TODO: add support to register falling edges
// GW/20220310 add support for sending string markers
// GW/20220324 change ISR mechanism to Alert functions in pigpio 
//             -> much better latency en less missing events at high rates
// ----------- add support for changing the LSL name and type published on the network

#include "led.h"
#include "simplequeue.h"
#include "zmq_cmd.h"
#include <chrono> // std::chrono::seconds
#include <cstring>
#include <iostream>
#include <lsl_cpp.h>
#include <memory>
#include <iomanip>
//#include <boost/asio.hpp>
//#include <boost/asio/buffer.hpp>
#include <boost/program_options.hpp>
#include <mutex>
#include <pigpio.h>
#include <sched.h>
#include <signal.h>
#include <stdlib.h>
#include <thread> // std::this_thread::sleep_for
#include <unistd.h>
#include <sys/mman.h>
#include <zmq.hpp>
//#include "zhelpers.hpp"

//#define DEBUG
//#define IGNACIO

using namespace lsl;
using namespace std;
using namespace boost;

int verbose = 0;

string info_name_markers;
string info_type_markers;
string info_sourceid_markers;

void parse_options(int argc, const char* argv[])
{
    namespace po = boost::program_options;

    // Declare the supported options.
    po::options_description desc("Usage:");
    desc.add_options()
       ("help,h", "show this message")
       ("lsl-name-markers", po::value<string>(), "set the name of the LSL marker stream")
       ("lsl-type-markers", po::value<string>(), "set the type of the LSL marker stream")
       ("lsl-sourceid-markers", po::value<string>(), "set the sourceid of the LSL marker stream")
       ("verbose,v", "enable verbose output")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
       cout << desc << "\n";
       exit(0);
    }

    if (vm.count("verbose")) {
       verbose = 1;
    }

    if (vm.count("lsl-name-markers")) {
       info_name_markers = vm["lsl-name-markers"].as<string>();
    }
    else {
       char hostname[255];
       gethostname(hostname,255);
       ostringstream s;
       s << "Markers @ " << hostname;
       info_name_markers = s.str();
    }

    if (verbose)
       cout << "LSL name is: " << info_name_markers << endl;

    if (vm.count("lsl-type-markers"))
       info_type_markers = vm["lsl-type-markers"].as<string>();
    else
       info_type_markers = "Digital Markers";

    if (verbose)
       cout << "LSL type is: " << info_type_markers << endl;

    if (vm.count("lsl-sourceid-markers"))
       info_sourceid_markers = vm["lsl-sourceid-markers"].as<string>();
    else {
       char hostname[255];
       gethostname(hostname,255);
       ostringstream s;
       s << argv[0] << "@" << hostname;
       info_sourceid_markers = s.str();
    }
    if (verbose)
       cout << "LSL sourceid is: " << info_sourceid_markers << endl;
}

#ifdef LSLDER_1CHAN

// 1 channel portable version without LEDs
const int ngpio = 1;
const int gpioinp[ngpio] = {8};

const int default_debouncetime = 100;

const int *ledgpio = 0;
const int nled = 0;

#endif

#ifdef LSLDER_4CHAN

// 4-chan version with 4 LEDs
const int ngpio = 4;
const int gpioinp[ngpio] = {15, 18, 23, 24};

const int default_debouncetime = 0;

const int ledgpio[] = {2, 3, 14, 5};
const int nled = 4;

#endif

#ifdef LSLDER_8CHAN

// 8-chan version with 8 LEDs
const int ngpio = 8;
const int gpioinp[ngpio] = {12, 7, 8, 25, 24, 23, 18, 15};

const int default_debouncetime = 0;

const int ledgpio[] = {21, 20, 16, 13, 5, 14, 3, 2};
const int nled = 8;

#endif

const int maxgpio = 54; // BCM chip has 54 GPIO
int gpiomap[maxgpio];

void init_gpiomap() {
    // map BNC input channel wiring 0..ngpio-1 to BCM GPIO
    for (int bnc_input = 0; bnc_input < ngpio; bnc_input++) {
        int bcmgpio = gpioinp[bnc_input];
        gpiomap[bcmgpio] = bnc_input;
    }
}

string gpioMarker[ngpio][2];      // markers 1 M[n][0] for falling, M[n][1] for rising inputs
bool gpioMarkerEnabled[ngpio][2]; // markers 1 M[n][0] for falling, M[n][1] for rising inputs

void gpioMarker_set(int channel, int direction, const string& value)
{
   if (channel < 0 || channel >= ngpio)
      return;
   if (direction < 0 || direction > 1)
      return;
   gpioMarker[channel][direction] = value;
}

void gpioMarker_enable(int channel, int direction, bool enable = true)
{
   if (channel < 0 || channel >= ngpio)
      return;
   if (direction < 0 || direction > 1)
      return;
   gpioMarkerEnabled[channel][direction] = enable;
}

void gpioMarker_disable(int channel, int direction)
{
   gpioMarker_enable(channel, direction, false);
}


void gpioMarkers_init()
{
    for (int i = 0; i < ngpio; i++) {
        ostringstream s0;
        s0 << i << " FALL";
        gpioMarker[i][0] = s0.str();
    }
    for (int i = 0; i < ngpio; i++) {
        ostringstream s1;
        s1 << i << " RISE";
        gpioMarker[i][1] = s1.str();
    }
    for (int i = 0; i < ngpio; i++) {
       gpioMarkerEnabled[i][0] = false;
       gpioMarkerEnabled[i][1] = true;
    }
}


int ledtime = 2; // in 1/100 sec., see infinite loop in main()
volatile int ledtimer[ngpio] = {
    0,
};
int ledstatus[ngpio] = {
    0,
};

int debouncetimes[ngpio] = {
    0,
};
volatile int debouncetimer[ngpio] = {
    0,
};

#ifdef DEBUG
volatile int interrupt_count[ngpio] = {
    0,
};
#endif

stream_outlet *numeric_outlet[ngpio];
stream_outlet *marker_outlet;

std::mutex lsl_mutex;
typedef char int8;

volatile bool run = false;

void stop(int signum) { run = false; }

class qelement
{
public:
    double timestamp;
    int bnc_input;
    int edge;

    qelement()
    {
       timestamp = -1;
       bnc_input = -1;
       edge = -1;
    }

    qelement(double _timestamp, int _bnc_input, int _edge)
    {
       timestamp = _timestamp;
       bnc_input = _bnc_input;
       edge = _edge;
    }
};

queue<qelement> event_queue(10000);

void gpioISR(int gpio, int level, uint32_t tick) {
    uint32_t now = gpioTick();
    uint32_t offset = now - tick;
    double timestamp = lsl_local_clock() - double(offset)/1e6;

    if (!run)
        return;

    if (level == 2) // timeout call
        return;

    int bnc_input = gpiomap[gpio];

    if (debouncetimer[bnc_input])
        return;
    else
        debouncetimer[bnc_input] = debouncetimes[bnc_input];

    std::lock_guard<std::mutex> guard(lsl_mutex);
#ifdef DEBUG
    std::cout << "Interrupt on bnc_input " << bnc_input << " (GPIO "
              << gpioinp[bnc_input] << ")" << std::endl;
#endif

    if (!event_queue.isFull())
        event_queue.enqueue(qelement(timestamp, bnc_input, 1-level));
    else {
        for (int i = 0; (i < ngpio) && (i < nled); i++)
            ledtimer[i] = ledtime;
    }

    int edge = 1 - level;
    if (edge == 1) {   // old style behavior
        ledtimer[bnc_input] = ledtime;
    }

    if (edge == 0 || edge == 1) {
        if (gpioMarkerEnabled[bnc_input][edge]) {
            ledtimer[bnc_input] = ledtime;
        }
    }

#ifdef DEBUG
    interrupt_count[bnc_input]++;
    std::cout << bnc_input << " (count=" << interrupt_count[bnc_input] << ")"
              << std::endl;
#endif
}

void handleLeds() {
    for (int i = 0; (i < ngpio) && (i < nled); i++) {
        if (ledtimer[i] > 0) {
            if (!ledstatus[i]) {
                setled(i, 1);
                ledstatus[i] = 1;
            }
            ledtimer[i]--;
        }
        else {
            if (ledstatus[i]) {
                setled(i, 0);
                ledstatus[i] = 0;
            }
        }
    }
}

int rtpriority(int n) {
    int r = mlockall(MCL_CURRENT|MCL_FUTURE|MCL_ONFAULT);
    if (r < 0)
        return r;

    struct sched_param sched;

    memset(&sched, 0, sizeof(sched));

    if (n > sched_get_priority_max(SCHED_FIFO))
        sched.sched_priority = sched_get_priority_max(SCHED_FIFO);
    else
        sched.sched_priority = n;

    return sched_setscheduler(0, SCHED_FIFO, &sched);
}

int zmq_recv_multipart(zmq::socket_t &socket, zmq::message_t parts[], int nmax) {
    int64_t more;
    size_t more_size = sizeof more;
    int n = 0;
    try {
        do {
            if (n < nmax) {
                socket.recv(parts[n]);
                if (verbose)
                    cout << "recv part " << n << endl;
                n++;
            }
            else {
                zmq::message_t discard;
                socket.recv(discard);
                if (verbose)
                    cout << "recv part " << n << " (discarded)." << endl;
            }
            socket.getsockopt(ZMQ_RCVMORE, &more, &more_size);
        } while (more);
    }
    catch (std::exception &error) {
        cout << "zmq_rev_multipart while receiving frame " << n << ": "
             << error.what() << endl;
    }
    return n;
}


int main(int argc, const char *argv[]) {
    parse_options(argc, argv);
    init_gpiomap();
    gpioMarkers_init();

#ifdef IGNACIO
#warning Special for Ignacio
    gpioMarker_set(0, 1, "STIM");
    gpioMarker_set(1, 1, "STD");
    gpioMarker_set(2, 1, "DEV");
    gpioMarker_enable(3, 0, true);
    gpioMarker_enable(3, 1, false);
    gpioMarker_set(3, 0, "BTN");
#endif

    if (rtpriority(40) < 0) {
        perror("rtpriority");
        return 1;
    }

    // configure input sample rate
    int r = gpioCfgClock(5,1,0);   // User 5 us sample rate on GPIO pins
    if (r < 0) {
        perror("gpioCfgClock");
        return 1;
    }

    // configure input channels
    r = gpioInitialise(); // use Broadcom pin numbering
    if (r < 0) {
        perror("gpioInitialise");
        return 1;
    }

    gpioSetSignalFunc(SIGINT, stop);
    gpioSetSignalFunc(SIGABRT, stop);
    gpioSetSignalFunc(SIGTERM, stop);

    printf("Press control C to stop.\n");

    initleds(nled, ledgpio);

    char hostname[255];
    gethostname(hostname, 255);
    for (int i = 0; i < ngpio; i++) {
        // make a new stream_info and open an outlet with it
        char info_name_digital[255];
        sprintf(info_name_digital, "Digital Events %d", i);

        char info_type_digital[255 + 18];
        sprintf(info_type_digital, "Digital Events @ %s", hostname);

        stream_info info(info_name_digital, info_type_digital, 1, lsl::IRREGULAR_RATE,
                         lsl::cf_int8, "Raspberry Pi Digital Event Recorder");
        numeric_outlet[i] = new stream_outlet(info);
    }

    // make a new stream_info and open an outlet with it
    stream_info info_markers(
       info_name_markers.c_str(),
       info_type_markers.c_str(),
       1,
       lsl::IRREGULAR_RATE,
       lsl::cf_string,
       info_sourceid_markers.c_str()
    );
    marker_outlet = new stream_outlet(info_markers);


    for (int i = 0; i < ngpio; i++) {
        int bcmgpio = gpioinp[i]; // BCM GPIO number
        //if (gpioSetMode(bcmgpio, PI_INPUT) < 0) {
        //    perror("gpioSetMode");
        //    return 1;
        // }

        debouncetimes[i] = default_debouncetime;

        const int timeout_ms = 10000;
        // The 4N35 optocoupler in the input circuit inverts the signal. 
        // To detect rising edges in the input signal look at the falling input, and the other way around.
        //if (gpioSetISRFunc(bcmgpio, EITHER_EDGE, timeout_ms, gpioISR) < 0) {
        if (gpioSetAlertFunc(bcmgpio, gpioISR) < 0) {
            perror("gpioSetISRFunc");
            return 1;
        }
        if (gpioSetPullUpDown(bcmgpio, PI_PUD_UP) < 0) {
            perror("gpioSetMode");
            return 1;
        }

#ifdef DEBUG
        std::cout << "Set IRQ handler for gpio " << bcmgpio << std::endl;
#endif

    }

    //  Prepare our context and socket
    zmq::context_t context(1);
    zmq::socket_t srep(context, ZMQ_REP);
    srep.bind("tcp://*:5555");

    zmq::pollitem_t items[] = {
            {static_cast<void *>(srep), 0, ZMQ_POLLIN, 0},
    };


    run = true; 
    while (run) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        handleLeds();
        for (int i = 0; i < ngpio; i++) {
            if (debouncetimer[i] > 0)
                debouncetimer[i]--;
        }

        while (!event_queue.isEmpty()) {
            std::lock_guard<std::mutex> guard(lsl_mutex);
            qelement el = event_queue.peek();
            event_queue.dequeue();
            if (el.edge == 1) {
               // 'old style behaviour: only report rising edges (i.e. level == 0, inverted inputs!)
               numeric_outlet[el.bnc_input]->push_sample(&el.edge, el.timestamp);
            }
            
                if (gpioMarkerEnabled[el.bnc_input][el.edge]) {
                    marker_outlet->push_sample(&gpioMarker[el.bnc_input][el.edge], el.timestamp);
                }
            }
        }

        zmq::poll(items, 1, 1);
        if (items[0].revents & ZMQ_POLLIN) {
            // Read command from ZMQ socket
            const int maxmsg = 3;
            zmq::message_t msg[maxmsg];
            int nmsg = 0;
    
            if (verbose)
                cout << "polling" << endl;
    
            while (true) {
                // try to read a message from the sockets
                zmq::poll(items, 1, 1);
                if (items[0].revents & ZMQ_POLLIN) {
                    nmsg = zmq_recv_multipart(srep, msg, maxmsg);
                    zmq::const_buffer rep("OK\0", 4);
                    srep.send(rep);
                    if (verbose)
                        cout << "sent response back" << endl;
                }
                else {
                    // cout << "zmq::poll: unexpected event" << endl;
                    continue;
                }
    
                if (verbose)
                    cout << "done" << endl;
                break; 
            }
    
//GW: this only takes msg[0], not okay???
            zmq::message_t &request = msg[0];
            std::string str((char *)request.data());
            if (verbose) {
                cout << "Received: " << str << " (" << request.size()
                     << " bytes)" << endl;
                if (nmsg > 1) {
                    for (int i = 1; i < nmsg; i++)
                        cout << "  ...one more message, " << msg[i].size()
                             << " bytes" << endl;
                }
            }
    
    
            if (verbose)
                cout << "Received: " << str << endl;
    
            std::istringstream s(str);
            std::string cmd;
            s >> cmd;
    
            if (verbose)
               cout << "cmd=" << str << endl;
// ### HIERO GW

            if (cmd == "D") {
                int level;
                string marker;
                s >> level;
                s >> marker;
                level = (level != 0);
                if (verbose) {
                    cout << "level=" << level << endl;
                    cout << "marker=" << marker << endl;
                }
                ////std::lock_guard<std::mutex> guard(lsl_mutex);
                ////outlet->push_sample(&marker);
                ///// setdio(level);
            }
            else
            if (cmd == "I") {
               int mask, duration;
               string marker;
               s >> mask;
               s >> duration;
               s >> marker;
               if (verbose)  {
                  cout << "mask=" << mask << endl;
                  cout << "duration=" << duration << endl;
                  cout << "marker=" << marker << endl;
               }
               ///// std::lock_guard<std::mutex> guard(lsl_mutex);
               ///// outlet->push_sample(&marker);
               ///// pulseIR(mask, duration);
            }
            else if (cmd == "M") {
                string marker;
                s >> marker;
                if (verbose) {
                    cout << "marker=" << marker << endl;
                }
                ///// std::lock_guard<std::mutex> guard(lsl_mutex);
                ///// outlet->push_sample(&marker);
                ///// gpioInMarker = marker;
            }
 
        }
    }
    gpioTerminate();
    return 0;
}
