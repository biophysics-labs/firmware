#include "portaudio.h"
#include <cassert>
#include <cstdint>
#include <zmq.hpp>

class zmq_audiobuffer {

  public:
    std::uint32_t fsamp;
    std::uint32_t nsamp;
    std::uint32_t nchan;
    float *data;
    std::uint32_t idata;
    char message[20];
    int frames_per_buffer;

    zmq_audiobuffer(std::uint32_t _fsamp, std::uint32_t _nsamp,
                    std::uint32_t _nchan);
    ~zmq_audiobuffer();

    int fill();
    int fill(zmq::message_t&);
    int play();
    void stop();
    void abort();
    double duration() {
        return double(nsamp)/fsamp;
    }

    void dump(int nsamples);

    const zmq_audiobuffer *playing_now() {
       return zmq_audiobuffer::_playing_now;
    }
protected:
    bool playing;  // if this buffer is playing
    static zmq_audiobuffer *_playing_now;// points to any buffer that is playing,
                                         // else it's a nullptr
    PaStream *stream;

    static int stream_callback(const void *inputBuffer, void *outputBuffer,
                               unsigned long framesPerBuffer,
                               const PaStreamCallbackTimeInfo *timeInfo,
                               PaStreamCallbackFlags statusFlags,
                               void *userData);
    static void stream_finished(void *userData);
};


struct zmq_audio_metadata {
   std::uint32_t fsamp;
   std::uint32_t nsamp;
   std::uint32_t nchan;
   std::uint32_t _spare5;
   std::uint32_t _spare4;
   std::uint32_t _spare3;
   std::uint32_t _spare2;
   std::uint32_t _spare1;
};
