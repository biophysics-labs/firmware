//  Hello World server

#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

int main (void)
{
    //  Socket to talk to clients
    void *context = zmq_ctx_new ();
    void *responder = zmq_socket (context, ZMQ_REP);
    int rc = zmq_bind (responder, "tcp://*:5555");
    assert (rc == 0);
   printf("go!\n");

    while (1) {
        zmq_msg_t msg;
        int rc = zmq_msg_init (&msg);
        int n = zmq_msg_recv (&msg, responder, 0);
        printf ("Received Hello (%d bytes)\n",zmq_msg_size(&msg));
        sleep (1);          //  Do some 'work'
        zmq_send (responder, "World", 5, 0);
    }
    return 0;
}
