#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pigpio.h>

#if 1
const int left = 13;  // PWM1 audio to jack
const int right = 12; // PWM0 audio to jack
#else
const int left = 18;  // PWM1 audio to jack
const int right = 19; // PWM0 audio to jack
#endif
int pin = right;
int freq = 400;
int dt = 1000;
volatile int run;

void stop(int signum)
{
   run = 0;
}



int main(int argc,char *argv[])
{
    int r;
/*
    r = gpioCfgClock(10, 0, 0);
    if (r < 0)
    {
       perror("gpioCfgClock");
       return 1;
    }
*/

    r = gpioInitialise(); // use Broadcom pin numbering
    if (r < 0)
    {
       perror("gpioInitialise");
       return 1;
    }
    
    gpioSetSignalFunc(SIGINT, stop);

   printf("Press control C to stop.\n");

   if (gpioSetMode(pin, PI_ALT0) < 0) {
       perror("gpioSetMode");
       return 1;
   }

/*
   int d;
   if ((d=gpioPWM(pin, 128))<0) {
       perror("gpioPWM");
       return 1;
   }

   d=gpioGetPWMdutycycle(pin);

   int f;
   if ((f=gpioSetPWMfrequency(pin, freq)) < 0) {
       perror("gpioSetPWMfrequency");
       return 1;
   }
*/
   int d=500000;
   int f=freq;
   r=gpioHardwarePWM(pin, freq, d);
   if (r < 0)
   {
      perror("gpioHardwarePWM");
      return 1;
   }

   printf("PWM freq = %d, duty cycle = %d\n", f, d);

   run = 1;
   while(run)
   {
      time_sleep(0.01);
   }

   printf("\ntidying up\n");
   r=gpioHardwarePWM(pin, 0, 0);
   if (r < 0)
   {
      perror("gpioHardwarePWM");
      return 1;
   }
   gpioTerminate();

   // r = gpioCfgClock(5, 0, 0);
   // gpioInitialise();
   // gpioTerminate();
}
