//
// zmqaudio_pa.cc -- play audio samples received over zeromq sockets
// Copyright (c) 2020 Günter Windau
// based on the example in paex_sine.c
//

//
// This program uses the PortAudio Portable Audio Library.
// For more information see: http://www.portaudio.com/
// Copyright (c) 1999-2000 Ross Bencina and Phil Burk
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files
// (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
// ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//
// The text above constitutes the entire PortAudio license; however,
// the PortAudio community also makes the following non-binding requests:
//
// Any person wishing to distribute modifications to the Software is
// requested to send the modifications to the original developer so that
// they can be incorporated into the canonical version. It is also
// requested that these non-binding requests be included along with the
// license above.
//
#include "zmqaudio.h"
#include "portaudio.h"
#include "zhelpers.hpp"
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <sched.h>
#include <boost/program_options.hpp>

using namespace std;
using namespace boost;


#define NUM_SECONDS (5)
#define SAMPLE_RATE (44100)

#ifndef M_PI
#define M_PI (3.14159265)
#endif

#define TABLE_SIZE (200)
typedef struct {
    float sine[TABLE_SIZE];
    int left_phase;
    int right_phase;
    char message[20];
} paTestData;

string proxy;

zmq_audiobuffer *zmq_audiobuffer::_playing_now = nullptr;

int verbose = 0;
const int nrbuffers = 22;
zmq_audiobuffer *buffer[nrbuffers];

PaError print_error(PaError e) {
    if (e != paNoError)
        Pa_Terminate();
    fprintf(stderr, "An error occured while using the portaudio stream\n");
    fprintf(stderr, "Error number: %d\n", e);
    fprintf(stderr, "Error message: %s\n", Pa_GetErrorText(e));
    return e;
}

zmq_audiobuffer::zmq_audiobuffer(std::uint32_t _fsamp, std::uint32_t _nsamp,
                                 std::uint32_t _nchan)
    : fsamp{_fsamp}, nsamp{_nsamp}, nchan{_nchan},
      data{new float[_nsamp * _nchan]}, idata{0},
      frames_per_buffer{paFramesPerBufferUnspecified}, playing{false}, stream{nullptr} {
    assert(sizeof(float) == 4); // need 32 bit IEEE 754 floats
    //frames_per_buffer=64;
}

zmq_audiobuffer::~zmq_audiobuffer() {
    delete[] data;
    Pa_StopStream(stream);
    Pa_CloseStream(stream);
}

// This routine will be called by the PortAudio engine when audio is needed.
// It may called at interrupt level on some machines so don't do anything
// that could mess up the system like calling malloc() or free().
//
int zmq_audiobuffer::stream_callback(
    const void *, void *outputBuffer,
    unsigned long framesPerBuffer,
    const PaStreamCallbackTimeInfo *,
    PaStreamCallbackFlags, void *userData) {
    if (verbose)
        std::cerr << "<callback>" << std::flush;
    zmq_audiobuffer *pbuf = (zmq_audiobuffer *)userData;
    float *out = (float *)outputBuffer;
    unsigned long i;

    int ret = paContinue;
    for (i = 0; i < framesPerBuffer; i++) {
        if (pbuf->idata < (pbuf->nsamp * pbuf->nchan)) {
            for (int j = 0; j < pbuf->nchan; j++)
               *out++ = pbuf->data[pbuf->idata++];
        }
        else {
            for (int j = 0; j < pbuf->nchan; j++)
               *out++ = 0;
            ret = paComplete;
            //std::cerr << "</callback>" << std::endl;
            //return ret;
        }
    }
    if (verbose) {
        std::cerr << "idata=" << pbuf->idata << " fpb=" << framesPerBuffer << "  nsamp=" << pbuf->nsamp << std::flush;
        std::cerr << "</callback>" << std::endl;
    }
    return ret;
}

//
// This routine is called by portaudio when playback is done.
//
void zmq_audiobuffer::stream_finished(void *userData) {
    zmq_audiobuffer *pbuf = (zmq_audiobuffer *)userData;
    pbuf->playing = false;
    zmq_audiobuffer::_playing_now = nullptr;
    printf("Stream Completed: %s\n", pbuf->message);
}

int zmq_audiobuffer::play() {

    if (zmq_audiobuffer * p = zmq_audiobuffer::_playing_now) {
        std::cerr
          << "warning: zmq_audiobuffer::play: a buffer is already playing, stopping it"
          << std::endl;
        p->stop();
    }
    if (verbose)
        std::cerr << "Start!" << std::endl;
    zmq_audiobuffer::_playing_now = this;

    idata = 0;

    PaError err = Pa_Initialize();
    if (err != paNoError)
        return print_error(err);

    PaStreamParameters outputParameters;
    outputParameters.device =
        Pa_GetDefaultOutputDevice(); // default output device
    if (outputParameters.device == paNoDevice) {
        fprintf(stderr, "Error: No default output device.\n");
        return paNoDevice;
    }
    outputParameters.channelCount = nchan; // stereo output
    outputParameters.sampleFormat =
        paFloat32; // 32 bit floating point output
    outputParameters.suggestedLatency =
        Pa_GetDeviceInfo(outputParameters.device)->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    //PaError err;
    err = Pa_OpenStream(&stream, NULL, // no input
                        &outputParameters, fsamp, frames_per_buffer,
                        paClipOff, // we won't output out of range samples so
                                   // don't bother clipping them
                        zmq_audiobuffer::stream_callback, this);
    sprintf(message, "No Message");
    err =
        Pa_SetStreamFinishedCallback(stream, &zmq_audiobuffer::stream_finished);
    if (err != paNoError) {
        print_error(err);
        return err;
    }

    err = Pa_StartStream(stream);
    if (err != paNoError) {
        print_error(err);
        return err;
    }
    
    playing = true;

    //printf("Play for %d seconds.\n", NUM_SECONDS);
    //Pa_Sleep((unsigned int)(duration() * 1000));
    return paNoError;
}

void zmq_audiobuffer::abort() {
    if (verbose)
        std::cerr << "Abort!" << std::endl;
    Pa_AbortStream(stream);
    Pa_CloseStream(stream);
}

void zmq_audiobuffer::stop() {
    if (verbose)
        std::cerr << "Stop!" << std::endl;
    Pa_StopStream(stream);
    Pa_CloseStream(stream);
    Pa_Terminate();
}

int zmq_audiobuffer::fill() {
    // initialise sinusoidal wavetable
    const float fl{440};
    const float fr{880};
    const float vol{0.1};
    for (int i = 0; i < nsamp - 1; i++) {
        data[2 * i] = vol * sin((2 * M_PI * fl * i) / fsamp);
        data[2 * i + 1] = vol * sin((2 * M_PI * fr * i) / fsamp);
    }
    return 0;
}

int zmq_audiobuffer::fill(zmq::message_t &msg) {
    uint32_t ndata = msg.size()/sizeof(float);
    if (ndata != (nsamp*nchan)) {
        std::cerr <<
           "in int zmq_audiobuffer::fill(zmq::message_t &msg): audio data size mismatch" <<
           std::endl <<
           "ndata=" << ndata << std::endl <<
           "nsamp=" << nsamp << ", nchan=" << nchan << " total=" << nsamp*nchan <<
           std::endl;
        return 1;
    }
    std::memcpy(data, msg.data(), msg.size());
    return 0;
}

void zmq_audiobuffer::dump(int nsamples)
{
    std::cerr << "in void zmq_audiobuffer::dump(int nsamples):" << std::endl;
    std::cerr << "fsamp=" << fsamp << std::endl;
    std::cerr << "nsamp=" << nsamp << std::endl;
    std::cerr << "nchan=" << nchan << std::endl;
    std::cerr << "frames_per_buffer=" << frames_per_buffer << std::endl;
    std::cerr << "idata=" << idata << std::endl;
    for (int i=0; i<nsamples; i++) {
        std::cerr << "data[" << i << "]=" << data[i] << std::endl;
    }
}


int zmq_recv_multi(zmq::socket_t &socket, zmq::message_t parts[], int nmax) {
    int64_t more;
    size_t more_size = sizeof more;
    int n = 0;
    try {
        do {
            if (n < nmax) {
                socket.recv(&parts[n]);
                if (verbose)
                    std::cout << "recv part " << n << std::endl;
                n++;
            }
            else {
                zmq::message_t discard;
                socket.recv(&discard);
                if (verbose)
                    std::cout << "recv part " << n << " (discarded)."
                              << std::endl;
            }
            socket.getsockopt(ZMQ_RCVMORE, &more, &more_size);
        } while (more);
    }
    catch (std::exception &error) {
        std::cout << "zmq_rev_multi while receiving frame " << n << ": "
                  << error.what() << std::endl;
    }
    return n;
}

int rtpriority(int n) {
return 0;
    int sched_policy = SCHED_FIFO;
    struct sched_param sched;

    memset(&sched, 0, sizeof(sched));

    if (n > sched_get_priority_max(sched_policy))
        sched.sched_priority = sched_get_priority_max(sched_policy);
    else
        sched.sched_priority = n;

    return sched_setscheduler(0, sched_policy, &sched);
}

void parse_options(int argc, const char *argv[]) {
    namespace po = boost::program_options;

    // Declare the supported options.
    po::options_description desc("Usage:");
    desc.add_options()("help,h", "show this message")(
        "zmq-proxy,p", po::value<string>(), "select zmq pub-sub proxy host and port number, e.g. tcp://lsldert00.local:5557")(
        "verbose,v","enable verbose output");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        exit(0);
    }

    if (vm.count("verbose")) {
        verbose = 1;
    }

    if (vm.count("zmq-proxy"))
       proxy = vm["zmq-proxy"].as<string>();
    else
       proxy = "";

    if (verbose)
        cout << "ZMQ proxy is: " << proxy << endl;
}


int main(int argc, const char *argv[]) {

    int prio1 = 40;
    int prio2 = 50;

    parse_options(argc, argv);

    rtpriority(prio2);
    //PaError err = Pa_Initialize();
    //if (err != paNoError)
    //    return print_error(err);

    rtpriority(prio1);
    //  Prepare our context and socket
    zmq::context_t context(1);

    zmq::socket_t ssub(context, ZMQ_SUB);
    ssub.connect(proxy.c_str());
    //ssub.connect("tcp://lsldert00.local:5557");
    ssub.setsockopt(ZMQ_SUBSCRIBE, "A", 1);

    rtpriority(prio2);
    buffer[0] = new zmq_audiobuffer(44100, 5 * 48000, 2);
    buffer[0]->fill();

    int run = 1;
    while (run) {
        const int maxmsg = 3;
        zmq::message_t msg[maxmsg];
        int nmsg = 0;

        nmsg = zmq_recv_multi(ssub, msg, maxmsg);
        zmq::message_t &request = msg[0];
        std::string str((char *)request.data());

        std::uint32_t *audio_header = nullptr;
        if (nmsg > 2) {
            audio_header = new std::uint32_t[msg[1].size()/sizeof(std::uint32_t)];
            std::memcpy(audio_header, msg[1].data(), msg[1].size());
        }

        if (verbose) {
            std::cout << "Received: '" << str << "' (" << request.size()
                      << " bytes)" << std::endl;
            if (nmsg > 1) {
                for (int i = 1; i < nmsg; i++)
                    std::cout << "  ...one more message, " << msg[i].size()
                              << " bytes" << std::endl;
            }
        }

        // parse the command and data in the message
        std::istringstream s(str);
        std::string cmd;
        unsigned int ibuf;
        s >> cmd >> ibuf;

        if (verbose)
            std::cout << "cmd='" << cmd << "' ibuf=" << ibuf << std::endl;

        if (ibuf >= nrbuffers) {
            std::cerr << "buffer index out of range" << std::endl;
            continue;
        }

        if (cmd == "AF") { // Fill
            delete buffer[ibuf];
            
            if (audio_header) {
               std::uint32_t Fs = audio_header[0];
               std::uint32_t nchan = audio_header[1];
               std::uint32_t nsamp = audio_header[2];
               buffer[ibuf] = new zmq_audiobuffer(Fs, nsamp, nchan);
               buffer[ibuf]->fill(msg[2]);
               if (verbose)
                  buffer[ibuf]->dump(50);
            }
            else
               buffer[ibuf]=nullptr;
        }
        else if (cmd == "AP") { // Play
            if (buffer[ibuf]) {
               PaError err=buffer[ibuf]->play();
               if (err != paNoError) {
                   print_error(err);
                   return err;
               }
             }
        }
        else if (cmd == "AS") { // Stop
            if (buffer[ibuf]) {
               buffer[ibuf]->stop();
               if (verbose)
                  buffer[ibuf]->dump(4);
            }
        }

        delete[] audio_header;
        //delete[] audio_data;

        //buffer[0] = new zmq_audiobuffer(44100, 5 * 48000, 2);
        //zmq_audiobuffer *data = buffer[0];
        //data->fill();
        //printf("PortAudio Test: output sine wave. SR = %d, BufSize = %d\n",
               //SAMPLE_RATE, data->frames_per_buffer);

        //err = data->play();
        //if (err != paNoError)
            //return print_error(err);
    }
//    Pa_Terminate();
    printf("Test finished.\n");

    return 0;
    //return err;
}
