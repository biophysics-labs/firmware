//
//  zmq_trigger - version using PUB-SUB sockets
//  Binds SUB socket to tcp://lsldert00.local:5556
//  Expects "Hello" from client, replies with "World"
//
#include "zhelpers.hpp"
#include <boost/program_options.hpp>
#include <iomanip>
#include <iostream>
#include <lsl_cpp.h>
#include <memory>
#include <mutex>
#include <pigpio.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <unistd.h>

using namespace std;
using namespace boost;
using namespace lsl;

const int sched_policy = SCHED_FIFO;
const int sched_priority = 99;

const int chan_left = 13;  // PWM1 audio to jack
const int chan_right = 12; // PWM0 audio to jack

int pwmpin = chan_left;
int doutpin = 8;
int dinpin = 7;

int IRpin[] = { 14, 23 };  // GPIO pins of IR LED open collector drivers for fNIRS sync
const int IRnum = sizeof(IRpin)/sizeof(IRpin[0]);   // number of IR outputs
const int IRmask = (1<<IRnum)-1;  // the bit mask of the IR outputs
const int IR_ON = 1;
const int IR_OFF = 0;

stream_outlet *outlet;
string info_name, info_type, info_sourceid;
std::mutex lsl_mutex;


string proxy;

volatile int run = 1;

void stop(int signum) {
    printf("\nSignal %d caught. Stopping. Please wait...\n", signum);
    run = 0;
}

static int verbose = 0;

void parse_options(int argc, const char *argv[]) {
    namespace po = boost::program_options;

    // Declare the supported options.
    po::options_description desc("Usage:");
    desc.add_options()("help,h", "show this message")(
        "channel,c", po::value<char>(), "select channel [L or R]")(
        "zmq-proxy,p", po::value<string>(), "select zmq pub-sub proxy host and port number, e.g. tcp://lsldert00.local:5557")(
        "lsl-name", po::value<string>(), "set the name of the LSL stream")(
        "lsl-type", po::value<string>(), "set the type of the LSL stream")(
        "lsl-sourceid", po::value<string>(),
        "set the sourceid of the LSL stream")("verbose,v",
                                              "enable verbose output");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        exit(0);
    }

    if (vm.count("verbose")) {
        verbose = 1;
    }

    if (vm.count("zmq-proxy")) 
       proxy = vm["zmq-proxy"].as<string>();
    else
       proxy = "";

    if (verbose)
        cout << "ZMQ proxy is: " << proxy << endl;

    if (vm.count("lsl-name"))
        info_name = vm["lsl-name"].as<string>();
    else
        info_name = "Raspberry Pi Digital Triggers";

    if (verbose)
        cout << "LSL name is: " << info_name << endl;

    if (vm.count("lsl-type"))
        info_type = vm["lsl-type"].as<string>();
    else {
        char hostname[255];
        gethostname(hostname, 255);
        ostringstream s;
        s << "Digital Triggers @ " << hostname;
        info_type = s.str();
    }
    if (verbose)
        cout << "LSL type is: " << info_type << endl;

    if (vm.count("lsl-sourceid"))
        info_sourceid = vm["lsl-sourceid"].as<string>();
    else {
        char hostname[255];
        gethostname(hostname, 255);
        ostringstream s;
        s << argv[0] << "@" << hostname;
        info_sourceid = s.str();
    }
    if (verbose)
        cout << "LSL sourceid is: " << info_sourceid << endl;

    if (vm.count("channel")) {
        char c = vm["channel"].as<char>();
        switch (c) {
        case 'l':
        case 'L':
            pwmpin = chan_left;
            break;
        case 'r':
        case 'R':
            pwmpin = chan_right;
            break;
        default:
            cerr << "channel must be 'L' or 'R'" << endl;
            exit(1);
        }
    }
}

std::string str2hex(const std::string &s) {
    const static char str2hex_lookup[] = "0123456789abcdef";
    unsigned int i = 0, leng = s.length();
    std::stringstream r;
    for (i = 0; i < leng; i++) {
        r << str2hex_lookup[s[i] >> 4];
        r << str2hex_lookup[s[i] & 0x0f];
    }
    return r.str();
}

volatile uint32_t last_tick = 0;
uint32_t steady_ticks = 15000;

uint32_t tickdelta(uint32_t t2, uint32_t t1) {
    if (t2 > t1)
        return t2 - t1;
    else
        return UINT32_MAX - (t1 - t2);
}

string gpioInMarker = "DIN";

void gpioISR(int gpio, int level, uint32_t tick) {
    if (level == 2) // timeout call
        return;

    // check if in debounce period
    if ((last_tick != 0) && (tickdelta(tick, last_tick) < steady_ticks))
        return;

    last_tick = tick;
    if (last_tick ==
        0)             // treat 0 as a special value, i.e. it is not initialized
        last_tick = 1; // so we're 1 us off now, but that's not critical

    std::lock_guard<std::mutex> guard(lsl_mutex);
    outlet->push_sample(&gpioInMarker);
    if (verbose)
        cout << gpioInMarker << endl;
}

void setdio(int value) {
    int r = gpioWrite(doutpin, value);
    if (r < 0) {
        perror("gpioWrite");
        exit(1);
    }
}

void pulseIR(int mask, int durationmsec)
{
   for (int i=0; i< IRnum; i++)
   {
      int r=gpioWrite(IRpin[i], IR_ON);
      if (r < 0)
      {
         perror("gpioWrite");
         exit(1);
      }
   }
   std::this_thread::sleep_for(std::chrono::milliseconds(durationmsec));

   for (int i=0; i< IRnum; i++)
   {
      int r=gpioWrite(IRpin[i], IR_OFF);
      if (r < 0)
      {
         perror("gpioWrite");
         exit(1);
      }
   }
}

void beep(int freq, int durationmsec) {
    int d = 500000;
    int r = gpioHardwarePWM(pwmpin, freq, d);
    if (r < 0) {
        perror("gpioHardwarePWM");
        exit(1);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(durationmsec));

    r = gpioHardwarePWM(pwmpin, 0, 0);
    if (r < 0) {
        perror("gpioHardwarePWM");
        exit(1);
    }
}

int rtpriority(int n) {
    struct sched_param sched;

    memset(&sched, 0, sizeof(sched));

    if (n > sched_get_priority_max(sched_policy))
        sched.sched_priority = sched_get_priority_max(sched_policy);
    else
        sched.sched_priority = n;

    return sched_setscheduler(0, sched_policy, &sched);
}

int initialize_gpio() {
    // Prepare sound output
    int r = gpioInitialise(); // use Broadcom pin numbering
    if (r < 0) {
        perror("gpioInitialise");
        return r;
    }

    printf("Press control C to stop.\n");

    gpioSetSignalFunc(SIGINT, stop);
    gpioSetSignalFunc(SIGTERM, stop);
    gpioSetSignalFunc(SIGQUIT, stop);
    gpioSetSignalFunc(SIGABRT, stop);

    if ((r = gpioSetMode(chan_right, PI_ALT0)) < 0) {
        perror("gpioSetMode");
        return r;
    }

    if ((r = gpioSetMode(chan_left, PI_ALT0)) < 0) {
        perror("gpioSetMode");
        return r;
    }

    if ((r = gpioSetMode(doutpin, PI_OUTPUT)) < 0) {
        perror("gpioSetMode");
        return r;
    }

    if ((r = gpioSetPullUpDown(doutpin, PI_PUD_UP)) < 0) {
        perror("gpioSetMode");
        return r;
    }

    for (int i; i<IRnum; i++) {
        if (gpioSetMode(IRpin[i], PI_OUTPUT) < 0) {
            perror("gpioSetMode");
            return 1;
        }

        if (gpioSetPullUpDown(IRpin[i], PI_PUD_DOWN) < 0) {
            perror("gpioSetMode");
            return 1;
        }
    }

    if ((r = gpioSetMode(dinpin, PI_OUTPUT)) < 0) {
        perror("gpioSetMode");
        return r;
    }

    if ((r = gpioSetPullUpDown(dinpin, PI_PUD_UP)) < 0) {
        perror("gpioSetMode");
        return r;
    }

    if ((r = gpioSetISRFunc(dinpin, FALLING_EDGE, 10000, gpioISR)) < 0) {
        perror("gpioSetISRFunc");
        return r;
    }
    return 0;
}

int zmq_recv_multi(zmq::socket_t &socket, zmq::message_t parts[], int nmax) {
    int64_t more;
    size_t more_size = sizeof more;
    int n = 0;
    try {
        do {
            if (n < nmax) {
                socket.recv(&parts[n]);
                if (verbose)
                    cout << "recv part " << n << endl;
                n++;
            }
            else {
                zmq::message_t discard;
                socket.recv(&discard);
                if (verbose)
                    cout << "recv part " << n << " (discarded)." << endl;
            }
            socket.getsockopt(ZMQ_RCVMORE, &more, &more_size);
        } while (more);
    }
    catch (std::exception &error) {
        cout << "zmq_rev_multi while receiving frame " << n << ": "
             << error.what() << endl;
    }
    return n;
}

int main(int argc, const char *argv[]) {
    try {
        parse_options(argc, argv);

        if (rtpriority(sched_priority) < 0) {
            perror("rtpriority");
            return 1;
        }

        if (initialize_gpio() < 0) {
            return 1;
        }

        //  Prepare our context and socket
        zmq::context_t context(1);

        zmq::socket_t ssub(context, ZMQ_SUB);
        if (!proxy.empty())
           ssub.connect(proxy.c_str());
        //ssub.connect("tcp://lsldert00.local:5557");
        ssub.setsockopt(ZMQ_SUBSCRIBE, "b", 0);
        ssub.setsockopt(ZMQ_SUBSCRIBE, "B", 0);
        ssub.setsockopt(ZMQ_SUBSCRIBE, "D", 0);
        ssub.setsockopt(ZMQ_SUBSCRIBE, "I", 0);
        ssub.setsockopt(ZMQ_SUBSCRIBE, "M", 0);

        zmq::socket_t srep(context, ZMQ_REP);
        srep.bind("tcp://*:5555");

        zmq::pollitem_t items[] = {
            {static_cast<void *>(srep), 0, ZMQ_POLLIN, 0},
            {static_cast<void *>(ssub), 0, ZMQ_POLLIN, 0}};

        // make a new stream_info and open an outlet with it
        stream_info info(info_name.c_str(), info_type.c_str(), 1,
                         lsl::IRREGULAR_RATE, lsl::cf_string,
                         info_sourceid.c_str());
        outlet = new stream_outlet(info);

        while (run) {
            const int maxmsg = 3;
            zmq::message_t msg[maxmsg];
            int nmsg = 0;

            if (verbose)
                cout << "polling" << endl;

            while (true) {
                // try to read a message from the sockets
                zmq::poll(items, 2, 1);
                if (items[0].revents & ZMQ_POLLIN) {
                    nmsg = zmq_recv_multi(srep, msg, maxmsg);
                    const char *rep = "OK\0";
                    srep.send(rep, 3);
                    if (verbose)
                        cout << "sent response back" << endl;
                }
                else if (items[1].revents & ZMQ_POLLIN) {
                    nmsg = zmq_recv_multi(ssub, msg, maxmsg);
                }
                else {
                    // cout << "zmq::poll: unexpected event" << endl;
                    continue;
                }

                if (verbose)
                    cout << "done" << endl;
                break;
            }

            zmq::message_t &request = msg[0];
            std::string str((char *)request.data());
            if (verbose) {
                cout << "Received: " << str << " (" << request.size()
                     << " bytes)" << endl;
                if (nmsg > 1) {
                    for (int i = 1; i < nmsg; i++)
                        cout << "  ...one more message, " << msg[i].size()
                             << " bytes" << endl;
                }
            }

            // parse the command and data in the message
            std::istringstream s(str);
            std::string cmd;
            s >> cmd;

            if (verbose)
                cout << "cmd=" << str << endl;

            if (cmd == "b") { // Legacy command
                string marker = "BEEP";
                std::lock_guard<std::mutex> guard(lsl_mutex);
                outlet->push_sample(&marker);
                beep(400, 500);
            }
            else if (cmd == "B") {
                int freq, duration;
                string marker;
                s >> freq;
                s >> duration;
                s >> marker;
                if (verbose) {
                    cout << "freq=" << freq << endl;
                    cout << "duration=" << duration << endl;
                    cout << "marker=" << marker << endl;
                }
                std::lock_guard<std::mutex> guard(lsl_mutex);
                outlet->push_sample(&marker);
                beep(freq, duration);
            }
            else if (cmd == "D") {
                int level;
                string marker;
                s >> level;
                s >> marker;
                level = (level != 0);
                if (verbose) {
                    cout << "level=" << level << endl;
                    cout << "marker=" << marker << endl;
                }
                std::lock_guard<std::mutex> guard(lsl_mutex);
                outlet->push_sample(&marker);
                setdio(level);
            }
            else
            if (cmd == "I") {
               int mask, duration;
               string marker;
               s >> mask;
               s >> duration;
               s >> marker;
               if (verbose)  {
                  cout << "mask=" << mask << endl;
                  cout << "duration=" << duration << endl;
                  cout << "marker=" << marker << endl;
               }
               std::lock_guard<std::mutex> guard(lsl_mutex);
               outlet->push_sample(&marker);
               pulseIR(mask, duration);
            }
            else if (cmd == "M") {
                string marker;
                s >> marker;
                if (verbose) {
                    cout << "marker=" << marker << endl;
                }
                std::lock_guard<std::mutex> guard(lsl_mutex);
                outlet->push_sample(&marker);
                gpioInMarker = marker;
            }
            else if (cmd == "S") {
                string sound_cmd;
                int sound_arg;
                s >> sound_cmd;
                s >> sound_arg;
                if (verbose) {
                    cout << "sound_cmd=" << sound_cmd << endl;
                    cout << "sound_arg=" << sound_arg << endl;
                }
                //lsldert_sound_cmd(sound_cmd, sound_arg, msg, nmsg);
            }
        }
    }
    catch (zmq::error_t &error) {
        cout << "error: " << error.what() << endl;
    }
    catch (std::exception &error) {
        cout << "error: " << error.what() << endl;
    }

    printf("\nmopping up\n");
    int r = gpioHardwarePWM(pwmpin, 0, 0);
    if (r < 0) {
        perror("gpioHardwarePWM");
        return 1;
    }
    gpioTerminate();

    return 0;
}
