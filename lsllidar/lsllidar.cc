#define _POSIX_C_SOURCE 199309L
#include <linux/types.h>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include <lsl_cpp.h>
#include <boost/program_options.hpp>
#include <include/lidarlite_v3.h>
#include <sched.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>

using namespace std;
using namespace boost;
using namespace lsl;

const int sched_policy = SCHED_FIFO;
const int sched_priority = 99;

LIDARLite_v3 myLidarLite;

stream_outlet *outlet;
string info_name, info_type, info_sourceid;

static int verbose = 0;

int sample_interval_ms = 100;

void parse_options(int argc, const char *argv[]) {
    namespace po = boost::program_options;

    // Declare the supported options.
    po::options_description desc("Usage:");
    desc.add_options()("help,h", "show this message")(
        "sample-interval,i", po::value<int>(), "set the sample interval of the lidar sensor (in milliseconds)")(
        "lsl-name", po::value<string>(), "set the name of the LSL stream")(
        "lsl-type", po::value<string>(), "set the type of the LSL stream")(
        "lsl-sourceid", po::value<string>(),
        "set the sourceid of the LSL stream")("verbose,v",
                                              "enable verbose output");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        exit(0);
    }

    if (vm.count("verbose")) {
        verbose = 1;
    }

    if (vm.count("sample-interval")) {
        sample_interval_ms = vm["sample-interval"].as<int>();
    }

    if (verbose)
        cout << "sample interval is: " << sample_interval_ms << " ms" << endl;

    if (vm.count("lsl-name"))
        info_name = vm["lsl-name"].as<string>();
    else
        info_name = "Raspberry Pi LIDAR Events";

    if (verbose)
        cout << "LSL name is: " << info_name << endl;

    if (vm.count("lsl-type"))
        info_type = vm["lsl-type"].as<string>();
    else {
        char hostname[255];
        gethostname(hostname, 255);
        ostringstream s;
        s << "LIDAR Events @ " << hostname;
        info_type = s.str();
    }
    if (verbose)
        cout << "LSL type is: " << info_type << endl;

    if (vm.count("lsl-sourceid"))
        info_sourceid = vm["lsl-sourceid"].as<string>();
    else {
        char hostname[255];
        gethostname(hostname, 255);
        ostringstream s;
        s << argv[0] << "@" << hostname;
        info_sourceid = s.str();
    }

    if (verbose)
        cout << "LSL sourceid is: " << info_sourceid << endl;
}

int rtpriority(int n) {
    struct sched_param sched;

    memset(&sched, 0, sizeof(sched));

    if (n > sched_get_priority_max(sched_policy))
        sched.sched_priority = sched_get_priority_max(sched_policy);
    else
        sched.sched_priority = n;

    return sched_setscheduler(0, sched_policy, &sched);
}

static sigset_t timer_sig;

void waitfor_periodic_timer(void)
{
    int dummy;
    sigwait(&timer_sig, &dummy);
}

int start_periodic_timer(uint64_t offs, uint64_t period)
{
    struct itimerspec t;
    struct sigevent sigev;
    timer_t timer;
    const int signal = SIGRTMIN;
    int res;

    t.it_value.tv_sec     = offs    / 1000000;
    t.it_value.tv_nsec    = (offs   % 1000000) * 1000;
    t.it_interval.tv_sec  = period  / 1000000;
    t.it_interval.tv_nsec = (period % 1000000) * 1000;

    sigemptyset(&timer_sig);
    sigaddset(&timer_sig, signal);
    sigprocmask(SIG_BLOCK, &timer_sig, NULL);

    memset(&sigev, 0, sizeof(struct sigevent));
    sigev.sigev_notify = SIGEV_SIGNAL;
    sigev.sigev_signo = signal;
    res = timer_create(CLOCK_REALTIME, &sigev, &timer);
    if (res < 0) {
        perror("Timer Create");
        exit(-1);
    }
    return timer_settime(timer, 0 /*TIMER_ABSTIME*/, &t, NULL);
}

int main(int argc, const char *argv[])
{
    parse_options(argc, argv);

    if (rtpriority(sched_priority) < 0) {
        perror("rtpriority");
        return 1;
    }

    // Initialize i2c peripheral in the cpu core
    myLidarLite.i2c_init();

    // Optionally configure LIDAR-Lite
    myLidarLite.configure(3);  // 3 = max range

    // make a new stream_info and open an outlet with it
    stream_info info(info_name.c_str(), info_type.c_str(), 1,
                     lsl::IRREGULAR_RATE, lsl::cf_double64,
                     info_sourceid.c_str());
    outlet = new stream_outlet(info);

    start_periodic_timer(1000000,sample_interval_ms*1000);

    while(1)
    {
        waitfor_periodic_timer();
        // Each time through the loop, check BUSY
        int busyFlag = myLidarLite.getBusyFlag();

        if (busyFlag != 0x00)
            continue;
        
        double distance;
        myLidarLite.takeRange();
        distance = double(myLidarLite.readDistance())/100;
        outlet->push_sample(&distance);
        if (verbose)
           printf("%.2f\n", distance);
    }
}

