#define _POSIX_C_SOURCE 199309L
#include <sys/time.h>
#include <signal.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>


static sigset_t sig;

void wait_periodic_timer(void)
{
    int dummy;
    sigwait(&sig, &dummy);
}

int start_periodic_timer(uint64_t offs, uint64_t period)
{
    struct itimerspec t;
    struct sigevent sigev;
    timer_t timer;
    const int signal = SIGALRM;
    int res;

    t.it_value.tv_sec     = offs    / 1000000;
    t.it_value.tv_nsec    = (offs   % 1000000) * 1000;
    t.it_interval.tv_sec  = period  / 1000000;
    t.it_interval.tv_nsec = (period % 1000000) * 1000;

    sigemptyset(&sig);
    sigaddset(&sig, signal);
    sigprocmask(SIG_BLOCK, &sig, NULL);

    memset(&sigev, 0, sizeof(struct sigevent));
    sigev.sigev_notify = SIGEV_SIGNAL;
    sigev.sigev_signo = signal;
    res = timer_create(CLOCK_REALTIME, &sigev, &timer);
    if (res < 0) {
        perror("Timer Create");
	exit(-1);
    }
    return timer_settime(timer, 0 /*TIMER_ABSTIME*/, &t, NULL);
}

static void void_handler(void)
{
    static int cnt;
    static uint64_t start;
    uint64_t t;
    struct timeval tv;

    if (start == 0) {
        gettimeofday(&tv, NULL);
	start = tv.tv_sec * 1000ULL + tv.tv_usec / 1000ULL;
    }
        
    gettimeofday(&tv, NULL);
    t = tv.tv_sec * 1000ULL + tv.tv_usec / 1000ULL;
    if (cnt && (cnt % 100) == 0) {
        printf("Avg time: %f\n", (double)(t - start) / (double)cnt);
    }
    cnt++;
}

int main()
{
    int res;

    res = start_periodic_timer(10000, 5000);
    if (res < 0) {
        perror("Start Periodic Timer");

        return -1;
    }

    while(1) {
        wait_periodic_timer();
        void_handler();
    }

    return 0;
}
