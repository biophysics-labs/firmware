# Firmware

## Description

This repository contains firmware for single board computer hardware specifically made for the biophysics experimental setups. We typically use Raspberry Pi and Arduino boards. 

For communication we use LabStreamingLayer (LSL), ZeroMQ and the Boost framework.
The LSL streams are generic, but are usually captured by the Matlab data acquisition software inthe biofysica reposistory.

ZeroMQ is used for sending data from Matlab to the SBC hardware, e.g. sending triggers, stimuli or settings.

The boost framework is convenient for serial communication, real time extensions, etc.


## Installation from scratch 

### Install Raspberry Pi OS (using installer)

Download the installer from https://www.raspberrypi.com/software/ and follow the instructions

 
### Install Raspberry Pi OS (manual method)

Download raspbian lite from https://www.raspberrypi.org/software/operating-systems/
and write the disk image to an SD card using the shell commands below, or use a utility like Balena Etcher.

For example: 2019-09-26-raspbian-buster-lite.zip
```
$ unzip 2019-09-26-raspbian-buster-lite.zip
Find out the location of your SD card, and dd the image to the raw device:
$ sudo dd bs=1m if=2019-09-26-raspbian-buster-lite.img of=/dev/rdiskX
```

If you want to do a headless installation, go to the BOOT partition of the SD card
and make an empty file named 'SSH'

```
$ cd /Volumes/boot
$ touch ssh
```

Put the SD card in your Raspberry Pi, connect a network cable and power it on.

If you want to do it on WiFi, first create /boot/wpa_supplicant.conf with
the correct information

```
-- content of /boot/wpa_supplicant.conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=US

network={
	ssid="your-network-service-set-identifier"
	psk="your-network-WPA/WPA2-security-passphrase"
	key_mgmt=WPA-PSK
}
--
```


```
$ ssh raspberrypi -l pi
Password: raspberry

$ export TERM=vt100
$ sudo raspi-config
enable SSH
set up a new password
maybe setup WiFi
maybe expand the filesystem, 
update the utility

Reboot and login again.
```

### Optionally install a real time kernel

https://github.com/kdoren/linux/wiki

### Install the firmware

```
$ sudo apt-get update
$ sudo apt-get install git libboost-all-dev cmake pigpio libzmq3-dev libasound2 libasound2-data libasound2-dev portaudio19-dev portaudio19-doc nlohmann-json3-dev lsof makeself
If apt-get update shows errors because the release changed (e.g. from stable to oldstable), execute the following command, then install the packages
$ apt-get update --allow-releaseinfo-change

$
$ cd
$ git clone --recurse-submodules https://gitlab.science.ru.nl/biophysics-labs/firmware.git

Forgot --recurse-submodules? Run this in the firmware directory:
$ git submodule update --init --recursive --remote

Building AZMQ may fail on a c++20 test program. As a workaround just overwrite it:
$ echo "int main() { return 0;}" > firmware/azmq/test/cpp20/socket/main.cpp

Build and install LabStreamingLayer and AZMQ
$ cd firmware
$ make libs
$ sudo make install

Build the firmware applications
$ make all

Make the firmware run upon reboot

$ sudo crontab -e
@reboot /home/pi/firmware/init.d/rc

In /home/pi/firmware/init.d for each host a specific rc file exists that will be called from the rc script
The script uses the name returned by the 'hostname' command, prependen with 'rc.'. E.g. if your raspberry's hostname is 'myraspberry', the script should be named 'rc.myraspberry'. 

### Link-local networking fix ###
If link-local networking doesn't work, this can be due to an issue with dhclient and NEtworkManager. The easy fix is to disable NetworkManager and use dhcpcd instead

$ sudo apt install dhcpcd
$ sudo systemctl enable dhcpcd
$ sudo systemctl disable NetworkManager

### Use a fixed DNS configuration on a readonly root filesystem.
cat << EOF >/etc/resolv.conf
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF

Add this line to /etc/dhcpcd.conf
nohook resolv.conf

### ALSA/Portaudio ###
If you use ALSA/Portaudio using an external USB sound card or dongle, put these lines in /etc/asound.conf:

defaults.pcm.card 1
defaults.ctl.card 1

To get rid of warnings when using Portaudio, edit /usr/share/alsa/alsa.conf and comment out some lines, e.g.:
#GW pcm.rear cards.pcm.rear
#GW pcm.center_lfe cards.pcm.center_lfe
#GW pcm.side cards.pcm.side
#GW pcm.hdmi cards.pcm.hdmi
#GW pcm.modem cards.pcm.modem
#GW pcm.phoneline cards.pcm.phoneline

To disable the raspberry's internal Broadcom 2835 audio interface, create a file /etc/modprobe.d/alsa-blacklist.conf with the line:
blacklist snd_bcm2835



```

## Make your SD card readonly (deprecated)

This works and prevents a corrupted root file system, but also breaks dhcpcd. Use an overlay file sytem instead
```
change /etc/fstab from:

proc            /proc           proc    defaults          0       0
PARTUUID=abcdef01-01  /boot           vfat    defaults,ro          0       2
PARTUUID=abcdef01-02  /               ext4    defaults,noatime  0       1
tmpfs /tmp tmpfs defaults,noatime 0 0
tmpfs /var/log tmpfs defaults,noatime,nosuid 0 0

to:
proc            /proc           proc    defaults          0       0
PARTUUID=abcdef01-01  /boot           vfat    defaults,ro          0       2
PARTUUID=abcdef01-02  /               ext4    defaults,ro,noatime  0       1
tmpfs /tmp tmpfs defaults,noatime 0 0
tmpfs /var/log tmpfs defaults,noatime,nosuid 0 0

Note the ro in the second line to mount the rootfs readonly
```
Adding submodules from other repositories:
E.g.:  git submodule add  https://github.com/sccn/labstreaminglayer.git labstreaminglayer
```

## Run raspi-config and enable the overlay filesystem.
Got to "Performance Options" -> "Overlay File System" and enable the overlay file system. In the following step select it to be read only.
This prevents the rootfs going corrupt when switching of or unplugging the raspberry pi power supply.

## Trouble with SSL on git clone on older raspberry pi (stretch)

Apparently timesyncd does not run correctly so the system time is off.
Install htpdate
```
sudo apt install htpdate
```

## Trouble woth point-to-point network connections in Raspberry Pi OS Bookworm

Bookworm uses NetworkManager to configure its eth0 interface. Apparently this breaks APIPA autoconfiguration adresses when using a point-to-point ethernet connection when no DHCP service is available. See nmcli-configure-falback-ip-address.sh for a workaround
## Firmware upgrade

To perform firmware updates the init script rc.upgrade calls a program firmware.run that is available on an inserted USB stick at boot time. See the upgrade directory for the Makefile creating the firmware.run program using the makeself utility from https://makeself.io
