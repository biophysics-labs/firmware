#!/bin/bash
# test if a USB disk is inserted at boot time, and if so, run a script.
set -v
PARTS=$(lsblk -l -n -o NAME,FSTYPE,LABEL,MOUNTPOINT | tr -s ' ' '\t' | grep vfat | grep -v EFI | grep -v / | cut -f 1)
for part in $PARTS
do
   echo "checking $part...."
   echo "Found a VFAT partition, mounting it"
   mount -t vfat -r "/dev/$part" /mnt
   ls -la /mnt
   echo "unmounting $part"
   umount /mnt
done
