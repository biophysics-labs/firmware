#!/bin/bash
set -v
cp 99-usb-stick.rules /etc/udev/rules.d &&
udevadm control --reload-rules &&
udevadm trigger
