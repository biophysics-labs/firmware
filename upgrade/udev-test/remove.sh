#!/bin/bash
set -v
rm -f /etc/udev/rules.d/99-usb-stick.rules  &&
udevadm control --reload-rules &&
udevadm trigger
