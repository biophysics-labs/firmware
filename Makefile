
LSL_BUILD_DIR=labstreaminglayer/build/install
LSL_INSTALL_DIR=/usr/local

SUBDIRS=ledbox lslder lsldert zmqmqbroker lslhider

INSTALL=install -v

all clean: $(SUBDIRS)

#install: libs/install/lsl libs/install/azmq libs/install/libevdev
install: libs/install/lsl libs/install/libevdev

$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)


libs: labstreaminglayer libevdev


labstreaminglayer:
	cd labstreaminglayer && \
	test -d build || (mkdir build && cd build && cmake ..) && \
	cd build && \
	$(MAKE) 
# Seems fixed since mid 2023
#&& \
#	$(MAKE) install

# broken since mid 2023?
azmq:
	cd azmq && \
	test -d build || (mkdir build && cd build && cmake ..) && \
        cd build && \
        $(MAKE)

libevdev:
	cd libevdev && \
	./autogen.sh && \
	$(MAKE)

libs/install/lsl:
	cd labstreaminglayer/build && $(MAKE) install

#NOMORE workaround to install LSL in system directories
libs/install/lsl-NOMORE:
	$(INSTALL) $(LSL_BUILD_DIR)/lib/liblsl*.so.* /usr/local/lib/
	ln -sf $(LSL_INSTALL_DIR)/lib/$$(basename $(LSL_BUILD_DIR)/lib/liblsl*.so.*) $(LSL_INSTALL_DIR)/lib/liblsl32.so
	$(INSTALL) $(LSL_BUILD_DIR)/include/*.* $(LSL_INSTALL_DIR)/include/
	test -d $(LSL_INSTALL_DIR)/include/lsl || sudo mkdir $(LSL_INSTALL_DIR)/include/lsl
	$(INSTALL) $(LSL_BUILD_DIR)/include/lsl/* $(LSL_INSTALL_DIR)/include/lsl/
	ldconfig

libs/install/azmq:
	$(MAKE) -C azmq/build install

libs/install/libevdev:
	$(MAKE) -C libevdev install
	ldconfig
	
release:
	$(MAKE) -C upgrade release
.PHONY: release labstreaminglayer azmq libevdev $(SUBDIRS)
